local RVCredits					= "silverq, rozbuska"
local RVLicense					= "MIT License"
local RVProjectURL				= "http://www.returnofreckoning.com/forum/viewtopic.php?f=11&t=4534"
local RVRecentUpdates			= 
"09.07.2015 - v1.14 Release\n"..
"\t- Project official site location has been changed\n"..
"\n"..
"25.08.2010 - v1.13 Release\n"..
"\t- Fixed an issue with showing the distances information\n"..
"\n"..
"25.07.2010 - v1.12 Release\n"..
"\t- Code clearance\n"..
"\t- Adaptation to the new distance system has been made\n"..
"\n"..
"24.02.2010 - v1.11 Release\n"..
"\t- Code clearance\n"..
"\t- Adapted to work with the RV Mods Manager v0.99"

RVF_Modern = RVAPI_Frame:Subclass()

local RVF_MODERN_HEALTHBAR_WIDTH_MAX = 40

--------------------------------------------------------------
-- function Initialize()
-- Description:
--------------------------------------------------------------
function RVF_Modern.Initialize()

	-- First step: register this template in the RVAPI_Frames - templates and frames manager
	RVAPI_Frames.API_RegisterTemplate("RVF_Modern", "Modern")

	-- Second step: define event handlers
	RegisterEventHandler(SystemData.Events.ALL_MODULES_INITIALIZED, "RVF_Modern.OnAllModulesInitialized")
end

--------------------------------------------------------------
-- function Shutdown()
-- Description:
--------------------------------------------------------------
function RVF_Modern.Shutdown()

	-- First step: unregister all events
	UnregisterEventHandler(SystemData.Events.ALL_MODULES_INITIALIZED, "RVF_Modern.OnAllModulesInitialized")

	-- Second step:
	RVAPI_Frames.API_UnRegisterTemplate("RVF_Modern")
end

--------------------------------------------------------------
-- function OnAllModulesInitialized()
-- Description: event ALL_MODULES_INITIALIZED
-- We can start working with the RVAPI just then we sure they are all initialized
-- and ready to provide their services
--------------------------------------------------------------
function RVF_Modern.OnAllModulesInitialized()

	-- Final step: register in the RV Mods Manager
	-- Please note the folowing:
	-- 1. always do this ON SystemData.Events.ALL_MODULES_INITIALIZED event
	-- 2. you don't need to add RVMOD_Manager to the dependency list
	-- 3. the registration code should be same as below, with your own function parameters
	-- 4. for more information please follow by project official site
	if RVMOD_Manager then
		RVMOD_Manager.API_RegisterAddon("RVF_Modern", RVF_Modern, RVF_Modern.OnRVManagerCallback)
	end
end

--------------------------------------------------------------
-- function OnRVManagerCallback
-- Description:
--------------------------------------------------------------
function RVF_Modern.OnRVManagerCallback(Self, Event, EventData)

	if	Event == RVMOD_Manager.Events.CREDITS_REQUESTED then

		return RVCredits

	elseif	Event == RVMOD_Manager.Events.LICENSE_REQUESTED then

		return RVLicense

	elseif	Event == RVMOD_Manager.Events.PROJECT_URL_REQUESTED then

		return RVProjectURL

	elseif	Event == RVMOD_Manager.Events.RECENT_UPDATES_REQUESTED then

		return RVRecentUpdates

	end
end

--------------------------------------------------------------
-- constructor Create()
-- Description:
--------------------------------------------------------------
function RVF_Modern:Create(frameName)

	-- First step: get an object from a parent method
	local frameObject = self:ParentCreate(frameName, "RVF_ModernFrameTemplate", "Root")

	-- Second step: define variables
	frameObject.eHealthPercent	= 100
	frameObject.eName			= L""
	frameObject.eLevel			= 0
	frameObject.eTier			= 0
	frameObject.eRangeMax		= RVAPI_Range.Ranges.MAX_RANGE
	frameObject.eLastRange		= -1
	frameObject.eCareerLine		= 0

	-- Third step: start animation
	AnimatedImageStartAnimation(frameName.."Animation", 0, true, false, 0)

	-- Fourth step: register events
	WindowRegisterEventHandler(frameName, SystemData.Events.USER_SETTINGS_CHANGED, "RVF_Modern.OnUserSettingsChanged")

	-- Final step: return object
	return frameObject
end

--------------------------------------------------------------
-- destructor Destroy()
-- Description:
--------------------------------------------------------------
function RVF_Modern:Destroy()

	local FrameWindow = self:GetFrameWindow()

	-- First step: unregister events
	WindowUnregisterEventHandler(FrameWindow, SystemData.Events.USER_SETTINGS_CHANGED)

	-- Final step: call parent destructor
	self:ParentDestroy()
end

--------------------------------------------------------------
-- function OnGetDefaultSettings()
-- Description:
--------------------------------------------------------------
function RVF_Modern:OnGetDefaultSettings()

	-- First step: collect all default settings
	local Settings = {
		showTargetName			= true,		-- show name label
		showTargetRange			= true,		-- show range label
		showTargetLevel			= true,		-- show target level
		showTargetCareer		= true,		-- show target career
		showTargetHealthPercent	= true,		-- show target health percent
		showTargetParenthesis	= true,		-- show target parenthesis
		showGradient			= true,		-- show gradient health bar
		showTargetCareerIcon	= true,		-- show career icon
		showTargetAnimation		= true,		-- show animation

		UseGlobalScale			= true,
		ScaleFactor				= 1,						-- scale factor
		Layer					= Window.Layers.DEFAULT,	-- set background layer as default Window.Layers, WindowSetLayer(self.name, layer), WindowGetLayer(self.name)

		HealthBarWidth			= 10,		-- healthBar width in pixels

		frameTintColor			= {R=1,		G=1,	B=1,	A=1},
		deadFrameTintColor		= {R=0.5,	G=0.5,	B=0.5,	A=1},
		careerIconTintColor		= {R=1,		G=1,	B=1,	A=1},
		deadCareerIconTintColor	= {R=0.25,	G=0.25,	B=0.25,	A=1},
		animationTintColor		= {R=1,		G=1,	B=1,	A=0},
		deadAnimationTintColor	= {R=0.25,	G=0.25,	B=0.25,	A=0},
	}

	-- Final step: return result
	return Settings
end

--------------------------------------------------------------
-- function OnInitializeSettingsWindow()
-- Description: 
--------------------------------------------------------------
function RVF_Modern:OnInitializeSettingsWindow()

	local SettingsWindow = self:GetSettingsWindow()

	-- First step: create window from template
	CreateWindowFromTemplate(SettingsWindow, "RVF_ModernSettingsTemplate", "Root")
	LabelSetText(SettingsWindow.."TitleBarText", L"Modern")
	ButtonSetText(SettingsWindow.."ButtonOK", L"OK")

	-- Second step: set General tab window
	ComboBoxAddMenuItem(SettingsWindow.."TabWGeneralLayers", towstring("Background Layer (0)"))
	ComboBoxAddMenuItem(SettingsWindow.."TabWGeneralLayers", towstring("Default Layer (1)"))
	ComboBoxAddMenuItem(SettingsWindow.."TabWGeneralLayers", towstring("Secondary Layer (2)"))
	ComboBoxAddMenuItem(SettingsWindow.."TabWGeneralLayers", towstring("Popup Layer (3)"))
	ComboBoxAddMenuItem(SettingsWindow.."TabWGeneralLayers", towstring("Overlay Layer (4)"))

	LabelSetText(SettingsWindow.."TabWGeneralLabelUseGlobalScale", L"Use Global UI Scale")
	LabelSetTextColor(SettingsWindow.."TabWGeneralLabelUseGlobalScale", 255, 255, 255)
	LabelSetText(SettingsWindow.."TabWGeneralSliderBarScaleLabel", L"Scale")

	-- Fourth step: set Frame tab window
	LabelSetText(SettingsWindow.."TabWFrameCaptionColor", L"Color")
	LabelSetTextColor(SettingsWindow.."TabWFrameCaptionColor", 255, 255, 255)

	LabelSetText(SettingsWindow.."TabWFrameLabelShowName", L"Show Name")
	LabelSetTextColor(SettingsWindow.."TabWFrameLabelShowName", 255, 255, 255)

	LabelSetText(SettingsWindow.."TabWFrameLabelShowLevel", L"Show Level")
	LabelSetTextColor(SettingsWindow.."TabWFrameLabelShowLevel", 255, 255, 255)

	LabelSetText(SettingsWindow.."TabWFrameLabelShowRange", L"Show Range")
	LabelSetTextColor(SettingsWindow.."TabWFrameLabelShowRange", 255, 255, 255)

	LabelSetText(SettingsWindow.."TabWFrameLabelShowGradient", L"Use gradient Health Bar")
	LabelSetTextColor(SettingsWindow.."TabWFrameLabelShowGradient", 255, 255, 255)

	LabelSetText(SettingsWindow.."TabWFrameLabelShowHealthPercent", L"Show Health Percentage")
	LabelSetTextColor(SettingsWindow.."TabWFrameLabelShowHealthPercent", 255, 255, 255)

	LabelSetText(SettingsWindow.."TabWFrameLabelShowParenthesis", L"Show Parenthesis")
	LabelSetTextColor(SettingsWindow.."TabWFrameLabelShowParenthesis", 255, 255, 255)

	LabelSetText(SettingsWindow.."TabWFrameSliderBarHealthBarWidthLabel", L"Health Bar Breadth")

	-- Fifth step: set CareerIcon tab window
	LabelSetText(SettingsWindow.."TabWCareerIconCaption", L"Career Icon")
	LabelSetText(SettingsWindow.."TabWCareerIconLabelEnabled", L"Enabled")
	LabelSetTextColor(SettingsWindow.."TabWCareerIconLabelEnabled", 255, 255, 255)

	-- Sixth step: set Animation tab window
	LabelSetText(SettingsWindow.."TabWAnimationCaption", L"Animation")
	LabelSetText(SettingsWindow.."TabWAnimationLabelEnabled", L"Enabled")
	LabelSetTextColor(SettingsWindow.."TabWAnimationLabelEnabled", 255, 255, 255)

	-- Third step: set tab buttons
	ButtonSetText(SettingsWindow.."TabGeneral", L"General")
	ButtonSetText(SettingsWindow.."TabFrame", L"Frame")
	ButtonSetText(SettingsWindow.."TabCareerIcon", L"Career icon")
	ButtonSetText(SettingsWindow.."TabAnimation", L"Animation")

	-- Final step: setup tabs visibility
	WindowSetShowing(SettingsWindow.."TabWGeneral", true)
	WindowSetShowing(SettingsWindow.."TabWFrame", false)
	WindowSetShowing(SettingsWindow.."TabWCareerIcon", false)
	WindowSetShowing(SettingsWindow.."TabWAnimation", false)

	ButtonSetPressedFlag(SettingsWindow.."TabGeneral", true)
	ButtonSetPressedFlag(SettingsWindow.."TabFrame", false)
	ButtonSetPressedFlag(SettingsWindow.."TabCareerIcon", false)
	ButtonSetPressedFlag(SettingsWindow.."TabAnimation", false)
end

--------------------------------------------------------------
-- function OnShowSettingsWindow()
-- Description:
--------------------------------------------------------------
function RVF_Modern:OnShowSettingsWindow()

	local CurrentSettings	= self:GetSettings()
	local SettingsWindow	= self:GetSettingsWindow()

	-- First step: update General TAB
	ButtonSetPressedFlag(SettingsWindow.."TabWGeneralCheckBoxUseGlobalScale", CurrentSettings.UseGlobalScale)
	SliderBarSetCurrentPosition(SettingsWindow.."TabWGeneralSliderBarScale", CurrentSettings.ScaleFactor-0.5)
	LabelSetText(SettingsWindow.."TabWGeneralSliderBarScaleEdit",	towstring(string.sub(CurrentSettings.ScaleFactor, 1, 3)))
	ComboBoxSetSelectedMenuItem(SettingsWindow.."TabWGeneralLayers", CurrentSettings.Layer+1)

	-- : update Frame TAB
	ButtonSetPressedFlag(SettingsWindow.."TabWFrameCheckBoxShowName", CurrentSettings.showTargetName)
	ButtonSetPressedFlag(SettingsWindow.."TabWFrameCheckBoxShowLevel", CurrentSettings.showTargetLevel)
	ButtonSetPressedFlag(SettingsWindow.."TabWFrameCheckBoxShowRange", CurrentSettings.showTargetRange)
	ButtonSetPressedFlag(SettingsWindow.."TabWFrameCheckBoxShowGradient", CurrentSettings.showGradient)
	ButtonSetPressedFlag(SettingsWindow.."TabWFrameCheckBoxShowHealthPercent", CurrentSettings.showTargetHealthPercent)
	ButtonSetPressedFlag(SettingsWindow.."TabWFrameCheckBoxShowParenthesis", CurrentSettings.showTargetParenthesis)
	WindowSetTintColor(SettingsWindow.."TabWFrameLiveColorBorderForeground", CurrentSettings.frameTintColor.R*255, CurrentSettings.frameTintColor.G*255, CurrentSettings.frameTintColor.B*255) 
	WindowSetTintColor(SettingsWindow.."TabWFrameDeadColorBorderForeground", CurrentSettings.deadFrameTintColor.R*255, CurrentSettings.deadFrameTintColor.G*255, CurrentSettings.deadFrameTintColor.B*255) 
	SliderBarSetCurrentPosition(SettingsWindow.."TabWFrameSliderBarHealthBarWidth", CurrentSettings.HealthBarWidth / RVF_MODERN_HEALTHBAR_WIDTH_MAX)
	LabelSetText(SettingsWindow.."TabWFrameSliderBarHealthBarWidthEdit",	towstring(CurrentSettings.HealthBarWidth)..L" pixels")

	-- : update Career Icon TAB
	ButtonSetPressedFlag(SettingsWindow.."TabWCareerIconCheckBoxEnabled", CurrentSettings.showTargetCareerIcon)
	WindowSetTintColor(SettingsWindow.."TabWCareerIconLiveColorBorderForeground", CurrentSettings.careerIconTintColor.R*255, CurrentSettings.careerIconTintColor.G*255, CurrentSettings.careerIconTintColor.B*255) 
	WindowSetTintColor(SettingsWindow.."TabWCareerIconDeadColorBorderForeground", CurrentSettings.deadCareerIconTintColor.R*255, CurrentSettings.deadCareerIconTintColor.G*255, CurrentSettings.deadCareerIconTintColor.B*255) 

	-- : update Animation TAB
	ButtonSetPressedFlag(SettingsWindow.."TabWAnimationCheckBoxEnabled", CurrentSettings.showTargetAnimation)
	WindowSetTintColor(SettingsWindow.."TabWAnimationLiveColorBorderForeground", CurrentSettings.animationTintColor.R*255, CurrentSettings.animationTintColor.G*255, CurrentSettings.animationTintColor.B*255) 
	WindowSetTintColor(SettingsWindow.."TabWAnimationDeadColorBorderForeground", CurrentSettings.deadAnimationTintColor.R*255, CurrentSettings.deadAnimationTintColor.G*255, CurrentSettings.deadAnimationTintColor.B*255) 
end

--------------------------------------------------------------
-- function OnSetSetting()
-- Description: set settings event
--------------------------------------------------------------
function RVF_Modern:OnSetSetting(SettingName, Value)

	local FrameWindow = self:GetFrameWindow()

	-- set frameTintColor, deadFrameTintColor
	if	SettingName == "frameTintColor" or
		SettingName == "deadFrameTintColor" then

		-- : update reticle
		self:UpdateReticle()
	end

	-- set careerIconTintColor, deadCareerIconTintColor
	if	SettingName == "careerIconTintColor" or
		SettingName == "deadCareerIconTintColor" then

		-- : update career icon
		self:UpdateCareerIcon()
	end

	-- set animationTintColor, deadAnimationTintColor
	if	SettingName == "animationTintColor" or
		SettingName == "deadAnimationTintColor" then

		-- : update animation
		self:UpdateAnimation()
	end

	-- set showTargetName
	if	SettingName == "showTargetName" then
		WindowSetShowing(FrameWindow.."FrameName", Value)
	end

	-- set showTargetLevel
	if	SettingName == "showTargetLevel" then

		-- : update name
		self:UpdateName()
	end

	-- set showTargetRange
	if	SettingName == "showTargetRange" then
		WindowSetShowing(FrameWindow.."FrameRange", Value)
	end

	-- set showTargetHealthPercent
	if	SettingName == "showTargetHealthPercent" then
		WindowSetShowing(FrameWindow.."FrameHealthPercentage", Value)
	end

	-- set showTargetParenthesis
	if	SettingName == "showTargetParenthesis" then
		WindowSetShowing(FrameWindow.."FrameReticle", Value)
	end

	-- set showTargetCareerIcon
	if	SettingName == "showTargetCareerIcon" then
		WindowSetShowing(FrameWindow.."CareerIcon", Value)
	end

	-- set showTargetAnimation
	if	SettingName == "showTargetAnimation" then
		WindowSetShowing(FrameWindow.."Animation", Value)
	end

	-- set ScaleFactor
	if	SettingName == "ScaleFactor" or
		SettingName == "UseGlobalScale" then

		-- set Scale
		self:UpdateScale()
	end

	-- set Layer
	if	SettingName == "Layer" then

		-- set Layer
		WindowSetLayer(FrameWindow, Value)
	end

	-- set healthBar width
	if	SettingName == "HealthBarWidth" then

		-- update health bar
		self:UpdateHealthBar()
	end

end

--------------------------------------------------------------
-- function GetSettingByWindow()
-- Description: 
--------------------------------------------------------------
function RVF_Modern:GetSettingByWindow(WindowName)

	local SettingsWindow = self:GetSettingsWindow()

	-- First step: calculate SettingName
	if WindowName == SettingsWindow.."TabWFrameLiveColor" then
		SettingName = "frameTintColor"
	elseif WindowName == SettingsWindow.."TabWFrameDeadColor" then
		SettingName = "deadFrameTintColor"
	elseif WindowName == SettingsWindow.."TabWCareerIconLiveColor" then
		SettingName = "careerIconTintColor"
	elseif WindowName == SettingsWindow.."TabWCareerIconDeadColor" then
		SettingName = "deadCareerIconTintColor"
	elseif WindowName == SettingsWindow.."TabWAnimationLiveColor" then
		SettingName = "animationTintColor"
	elseif WindowName == SettingsWindow.."TabWAnimationDeadColor" then
		SettingName = "deadAnimationTintColor"
	end

	-- Final step: return result
	return SettingName
end

-------------------------------------------------------------
-- Window Events
--------------------------------------------------------------

function RVF_Modern.CloseSettings()

	local self = RVAPI_Frames.API_GetFrameObjectBySettingsWindow(WindowGetParent(SystemData.ActiveWindow.name))
	self:HideFrameSettings()
end

function RVF_Modern.OnClickTabGeneral()

	local self				= RVAPI_Frames.API_GetFrameObjectBySettingsWindow(WindowGetParent(WindowGetParent(SystemData.ActiveWindow.name)))
	local SettingsWindow	= self:GetSettingsWindow()

	WindowSetShowing(SettingsWindow.."TabWGeneral", true)
	WindowSetShowing(SettingsWindow.."TabWFrame", false)
	WindowSetShowing(SettingsWindow.."TabWCareerIcon", false)
	WindowSetShowing(SettingsWindow.."TabWAnimation", false)

	ButtonSetPressedFlag(SettingsWindow.."TabGeneral", true)
	ButtonSetPressedFlag(SettingsWindow.."TabFrame", false)
	ButtonSetPressedFlag(SettingsWindow.."TabCareerIcon", false)
	ButtonSetPressedFlag(SettingsWindow.."TabAnimation", false)
end

function RVF_Modern.OnClickTabFrame()

	local self				= RVAPI_Frames.API_GetFrameObjectBySettingsWindow(WindowGetParent(WindowGetParent(SystemData.ActiveWindow.name)))
	local SettingsWindow	= self:GetSettingsWindow()

	WindowSetShowing(SettingsWindow.."TabWGeneral", false)
	WindowSetShowing(SettingsWindow.."TabWFrame", true)
	WindowSetShowing(SettingsWindow.."TabWCareerIcon", false)
	WindowSetShowing(SettingsWindow.."TabWAnimation", false)

	ButtonSetPressedFlag(SettingsWindow.."TabGeneral", false)
	ButtonSetPressedFlag(SettingsWindow.."TabFrame", true)
	ButtonSetPressedFlag(SettingsWindow.."TabCareerIcon", false)
	ButtonSetPressedFlag(SettingsWindow.."TabAnimation", false)
end

function RVF_Modern.OnClickTabCareerIcon()

	local self = RVAPI_Frames.API_GetFrameObjectBySettingsWindow(WindowGetParent(WindowGetParent(SystemData.ActiveWindow.name)))
	local SettingsWindow = self:GetSettingsWindow()

	WindowSetShowing(SettingsWindow.."TabWGeneral", false)
	WindowSetShowing(SettingsWindow.."TabWFrame", false)
	WindowSetShowing(SettingsWindow.."TabWCareerIcon", true)
	WindowSetShowing(SettingsWindow.."TabWAnimation", false)

	ButtonSetPressedFlag(SettingsWindow.."TabGeneral", false)
	ButtonSetPressedFlag(SettingsWindow.."TabFrame", false)
	ButtonSetPressedFlag(SettingsWindow.."TabCareerIcon", true)
	ButtonSetPressedFlag(SettingsWindow.."TabAnimation", false)
end

function RVF_Modern.OnClickTabAnimation()

	local self = RVAPI_Frames.API_GetFrameObjectBySettingsWindow(WindowGetParent(WindowGetParent(SystemData.ActiveWindow.name)))
	local SettingsWindow = self:GetSettingsWindow()

	WindowSetShowing(SettingsWindow.."TabWGeneral", false)
	WindowSetShowing(SettingsWindow.."TabWFrame", false)
	WindowSetShowing(SettingsWindow.."TabWCareerIcon", false)
	WindowSetShowing(SettingsWindow.."TabWAnimation", true)

	ButtonSetPressedFlag(SettingsWindow.."TabGeneral", false)
	ButtonSetPressedFlag(SettingsWindow.."TabFrame", false)
	ButtonSetPressedFlag(SettingsWindow.."TabCareerIcon", false)
	ButtonSetPressedFlag(SettingsWindow.."TabAnimation", true)
end

function RVF_Modern.OnComboBoxLayerChange( choiceIndex )

	local ComboBoxWindow = SystemData.ActiveWindow.name
	local self = RVAPI_Frames.API_GetFrameObjectBySettingsWindow(WindowGetParent(WindowGetParent(ComboBoxWindow)))

	self:SetSetting("Layer", choiceIndex-1)
end

function RVF_Modern.OnSlideScale( slidePos )

	local SliderWindow = SystemData.ActiveWindow.name
	local self = RVAPI_Frames.API_GetFrameObjectBySettingsWindow(WindowGetParent(WindowGetParent(SliderWindow)))

	local ScaleFactor	= slidePos + 0.5
	self:SetSetting("ScaleFactor", ScaleFactor)
	LabelSetText(SliderWindow.."Edit", towstring(string.sub(ScaleFactor, 1, 3)))
end

function RVF_Modern.OnSlideHealthBarWidth( slidePos )

	local SliderWindow = SystemData.ActiveWindow.name
	local self = RVAPI_Frames.API_GetFrameObjectBySettingsWindow(WindowGetParent(WindowGetParent(SliderWindow)))

	local HealthBarWidth = math.floor(slidePos * RVF_MODERN_HEALTHBAR_WIDTH_MAX)
	self:SetSetting("HealthBarWidth", HealthBarWidth)
	LabelSetText(SliderWindow.."Edit", towstring(HealthBarWidth)..L" pixels")
end

function RVF_Modern.OnMouseOverColorBox()

	local ColorBoxWindow	= SystemData.ActiveWindow.name
	local self				= RVAPI_Frames.API_GetFrameObjectBySettingsWindow(WindowGetParent(WindowGetParent(ColorBoxWindow)))
	local SettingsWindow	= self:GetSettingsWindow()

	Tooltips.CreateTextOnlyTooltip (ColorBoxWindow, nil)

	if ColorBoxWindow == SettingsWindow.."TabWFrameLiveColor" then
		Tooltips.SetTooltipText(1, 1, L"color")
	elseif ColorBoxWindow == SettingsWindow.."TabWFrameDeadColor" then
		Tooltips.SetTooltipText(1, 1, L"dead unit color")
	elseif ColorBoxWindow == SettingsWindow.."TabWCareerIconLiveColor" then
		Tooltips.SetTooltipText(1, 1, L"color")
	elseif ColorBoxWindow == SettingsWindow.."TabWCareerIconDeadColor" then
		Tooltips.SetTooltipText(1, 1, L"dead unit color")
	elseif ColorBoxWindow == SettingsWindow.."TabWAnimationLiveColor" then
		Tooltips.SetTooltipText(1, 1, L"color")
	elseif ColorBoxWindow == SettingsWindow.."TabWAnimationDeadColor" then
		Tooltips.SetTooltipText(1, 1, L"dead unit color")
	end
	Tooltips.SetTooltipColorDef(1, 1, Tooltips.COLOR_BODY)
	Tooltips.Finalize()

	local anchor = {Point="topleft", RelativeTo=ColorBoxWindow, RelativePoint="bottomleft", XOffset=0, YOffset=-8}
	Tooltips.AnchorTooltip(anchor)
	Tooltips.SetTooltipAlpha(1)
end

function RVF_Modern.OnLButtonUpColorBox()

	-- First step: get the colorbox windo name and the right self: object
	local ColorBoxWindow	= SystemData.ActiveWindow.name
	local self				= RVAPI_Frames.API_GetFrameObjectBySettingsWindow(WindowGetParent(WindowGetParent(ColorBoxWindow)))

	-- Second step: check for the current ColorBox
	if self.eCurrentColorBox == ColorBoxWindow then

		-- Third step: close color dialog if opened
		RVAPI_ColorDialog.API_CloseDialog(true)
	else

		-- Fourth step: calculate input data
		local SettingName		= self:GetSettingByWindow(ColorBoxWindow)
		local Color				= self:GetSettings()[SettingName]
		local SettingsWindow	= self:GetSettingsWindow()
		local Layer				= WindowGetLayer(SettingsWindow) + 1
		if Layer > Window.Layers.OVERLAY then
			Layer				= Window.Layers.OVERLAY
		end

		-- Fifth step: open color dialog window
		RVAPI_ColorDialog.API_OpenDialog(self, RVF_Modern.OnColorDialogCallback, true, Color.R*255, Color.G*255, Color.B*255, Color.A, Layer)

		-- Sixth step: save new ColorBox window
		self.eCurrentColorBox = ColorBoxWindow
	end
end

function RVF_Modern:OnColorDialogCallback(Event, EventData)

	-- First step: check for the current ColorBox
	if self.eCurrentColorBox == "" then
		return
	end

	-- Second step: calculate setting name
	local SettingName = self:GetSettingByWindow(self.eCurrentColorBox)

	-- Third step: check for the setting name
	if SettingName == "" then
		return
	end

	-- Fourth step: get color
	local Color	= self:GetSettings()[SettingName]

	-- Fifth step: check for the right event
	if Event == RVAPI_ColorDialog.Events.COLOR_EVENT_UPDATED then

		-- : set new color values
		WindowSetTintColor(self.eCurrentColorBox.."BorderForeground", EventData.Red, EventData.Green, EventData.Blue) 
		Color.R	= EventData.Red / 255
		Color.G	= EventData.Green / 255
		Color.B	= EventData.Blue / 255
		Color.A	= EventData.Alpha
		self:SetSetting(SettingName, Color)

	elseif Event == RVAPI_ColorDialog.Events.COLOR_EVENT_CLOSED then

		-- : clear the current ColorBox window name
		self.eCurrentColorBox = ""
	end
end

function RVF_Modern.SetShowName()

	local ActiveWindow		= SystemData.ActiveWindow.name
	local self				= RVAPI_Frames.API_GetFrameObjectBySettingsWindow(WindowGetParent(WindowGetParent(ActiveWindow)))
	local showTargetName	= self:GetSettings().showTargetName

	self:SetSetting("showTargetName", not showTargetName)
	ButtonSetPressedFlag(ActiveWindow, not showTargetName)
end

function RVF_Modern.SetShowLevel()

	local ActiveWindow		= SystemData.ActiveWindow.name
	local self				= RVAPI_Frames.API_GetFrameObjectBySettingsWindow(WindowGetParent(WindowGetParent(ActiveWindow)))
	local showTargetLevel	= self:GetSettings().showTargetLevel

	self:SetSetting("showTargetLevel", not showTargetLevel)
	ButtonSetPressedFlag(ActiveWindow, not showTargetLevel)
end

function RVF_Modern.SetShowRange()

	local ActiveWindow		= SystemData.ActiveWindow.name
	local self				= RVAPI_Frames.API_GetFrameObjectBySettingsWindow(WindowGetParent(WindowGetParent(ActiveWindow)))
	local showTargetRange	= self:GetSettings().showTargetRange

	self:SetSetting("showTargetRange", not showTargetRange)
	ButtonSetPressedFlag(ActiveWindow, not showTargetRange)
end

function RVF_Modern.SetShowGradient()

	local ActiveWindow	= SystemData.ActiveWindow.name
	local self			= RVAPI_Frames.API_GetFrameObjectBySettingsWindow(WindowGetParent(WindowGetParent(ActiveWindow)))
	local showGradient	= self:GetSettings().showGradient

	self:SetSetting("showGradient", not showGradient)
	ButtonSetPressedFlag(ActiveWindow, not showGradient)
end

function RVF_Modern.SetShowHealthPercent()

	local ActiveWindow				= SystemData.ActiveWindow.name
	local self						= RVAPI_Frames.API_GetFrameObjectBySettingsWindow(WindowGetParent(WindowGetParent(ActiveWindow)))
	local showTargetHealthPercent	= self:GetSettings().showTargetHealthPercent

	self:SetSetting("showTargetHealthPercent", not showTargetHealthPercent)
	ButtonSetPressedFlag(ActiveWindow, not showTargetHealthPercent)
end

function RVF_Modern.SetShowParenthesis()

	local ActiveWindow			= SystemData.ActiveWindow.name
	local self					= RVAPI_Frames.API_GetFrameObjectBySettingsWindow(WindowGetParent(WindowGetParent(ActiveWindow)))
	local showTargetParenthesis	= self:GetSettings().showTargetParenthesis

	self:SetSetting("showTargetParenthesis", not showTargetParenthesis)
	ButtonSetPressedFlag(ActiveWindow, not showTargetParenthesis)
end

function RVF_Modern.SetUseGlobalScale()

	local ActiveWindow		= SystemData.ActiveWindow.name
	local self				= RVAPI_Frames.API_GetFrameObjectBySettingsWindow(WindowGetParent(WindowGetParent(ActiveWindow)))
	local UseGlobalScale	= self:GetSettings().UseGlobalScale

	self:SetSetting("UseGlobalScale", not UseGlobalScale)
	ButtonSetPressedFlag(ActiveWindow, not UseGlobalScale)
end

function RVF_Modern.SetCareerIconEnabled()

	local ActiveWindow			= SystemData.ActiveWindow.name
	local self					= RVAPI_Frames.API_GetFrameObjectBySettingsWindow(WindowGetParent(WindowGetParent(ActiveWindow)))
	local showTargetCareerIcon	= self:GetSettings().showTargetCareerIcon

	self:SetSetting("showTargetCareerIcon", not showTargetCareerIcon)
	ButtonSetPressedFlag(ActiveWindow, not showTargetCareerIcon)
end

function RVF_Modern.SetAnimationEnabled()

	local ActiveWindow			= SystemData.ActiveWindow.name
	local self					= RVAPI_Frames.API_GetFrameObjectBySettingsWindow(WindowGetParent(WindowGetParent(ActiveWindow)))
	local showTargetAnimation	= self:GetSettings().showTargetAnimation

	self:SetSetting("showTargetAnimation", not showTargetAnimation)
	ButtonSetPressedFlag(ActiveWindow, not showTargetAnimation)
end

function RVF_Modern.OnVerticalBarSizeUpdated()

	local self = RVAPI_Frames.API_GetFrameObjectByFrameWindow(WindowGetParent(WindowGetParent(SystemData.ActiveWindow.name)))
	self:UpdateHealthBar()
end

function RVF_Modern.OnUserSettingsChanged()

	local self = RVAPI_Frames.API_GetFrameObjectByFrameWindow(SystemData.ActiveWindow.name)
	self:UpdateScale()
end

--------------------------------------------------------------
-- function OnEntityUpdated()
-- Description: entity updated event
--------------------------------------------------------------
function RVF_Modern:OnEntityUpdated(EntityData)

	-- First step: set new values
	self.eHealthPercent	= EntityData.HitPointPercent
	self.eName			= EntityData.Name
	self.eLevel			= EntityData.Level
	self.eTier			= EntityData.Tier
	self.eRangeMax		= EntityData.RangeMax
	self.eCareerLine	= EntityData.CareerLine

	-- Second step: update name
	self:UpdateName()

	-- Third step: update range
	self:UpdateRange()

	-- Fourth step: update reticle
	self:UpdateReticle()

	-- Fifth step: update health bar
	self:UpdateHealthBar()

	-- Sixth step: career icon
	self:UpdateCareerIcon()

	-- Seventh step: update animation
	self:UpdateAnimation()
end

--------------------------------------------------------------
-- function UpdateName()
-- Description:
--------------------------------------------------------------
function RVF_Modern:UpdateName()

	-- First step: get locals
	local Name			= self.eName or L""
	local Level			= self.eLevel or 0
	local Tier			= self.eTier or 0
	local FrameWindow	= self:GetFrameWindow()

	-- Second step: check for showTargetLevel flag
	if self:GetSettings().showTargetLevel then

		-- Third step: add more information
		Name = Name..L" ("..Level..( (Tier == 1 and L"+") or (Tier == 2 and L"++") or (Tier == 3 and L"+++") or L"" )..L")"
	end

	-- Final step: send a name value to window
	LabelSetText(FrameWindow.."FrameName", Name)
end

--------------------------------------------------------------
-- function UpdateRange()
-- Description:
--------------------------------------------------------------
function RVF_Modern:UpdateRange()

	-- First step: get locals
	local range			= self.eRangeMax or RVAPI_Range.Ranges.MAX_RANGE
	local FrameWindow	= self:GetFrameWindow()

	-- Second step: check range with a last value
	if range ~= self.eLastRange then

		-- : define locals
		local Width		= 0
		local Height	= 0
		local Message	= L""

		-- : calculate dimensions
		if range > 200 then
			Width		= 42
			Height		= 64
		elseif range <= 5 then
			Width		= 118
			Height		= 178
		else
			Width		= 120-0.4*range
			Height		= 180-0.6*range
		end

		-- : define range message
		if range < RVAPI_Range.Ranges.MAX_RANGE then
			Message	= towstring(range)..L" ft"
		end

		-- : update information
		WindowSetDimensions(FrameWindow.."Frame", Width, Height)
		LabelSetText(FrameWindow.."FrameRange", Message)

		-- : save the current range
		self.eLastRange = range
	end
end

--------------------------------------------------------------
-- function UpdateScale()
-- Description:
--------------------------------------------------------------
function RVF_Modern:UpdateScale()

	local FrameWindow = self:GetFrameWindow()

	-- First step: get scale factor
	local ScaleFactor
	if self:GetSettings().UseGlobalScale then
		ScaleFactor = InterfaceCore.GetScale()
	else
		ScaleFactor = self:GetSettings().ScaleFactor or 1
	end

	-- Second step: apply scale factor
	WindowSetScale(FrameWindow, ScaleFactor)
	WindowSetScale(FrameWindow.."FrameBar", 1)
	WindowSetScale(FrameWindow.."FrameBarInner", 1)
end

--------------------------------------------------------------
-- function UpdateReticle()
-- Description:
--------------------------------------------------------------
function RVF_Modern:UpdateReticle()

	local FrameWindow = self:GetFrameWindow()

	-- First step: get tint color by health
	local healthPercent = self.eHealthPercent or 100
	local TintColor
	if healthPercent > 0 then
		TintColor = self:GetSettings().frameTintColor
	else
		TintColor = self:GetSettings().deadFrameTintColor
	end

	-- Second step: check for TintColor
	if TintColor == nil then
		return
	end

	-- Third step: get RGBA components
	local R,G,B,A = math.floor(TintColor.R*255), math.floor(TintColor.G*255), math.floor(TintColor.B*255), TintColor.A

	-- Fourth step: set tint color
	WindowSetTintColor(FrameWindow.."Frame", R, G, B)
	WindowSetAlpha(FrameWindow.."Frame", A)

	-- Fifth step: set labels color
	LabelSetTextColor(FrameWindow.."FrameName", R, G, B)
	LabelSetTextColor(FrameWindow.."FrameRange", R, G, B)
	LabelSetTextColor(FrameWindow.."FrameHealthPercentage", R, G, B)

	-- Sixth step: check for gradient status and set foreground color
	if self:GetSettings().showGradient then
		-- default color if >0.75, red if <0.25, gradient in between
		local p = math.max(healthPercent/100*2-0.5, 0)
		if p < 1 then
			R, G, B = 255*(1-p)+R*p, p*G, p*B
		end
	end

	-- Seventh step: set health percentage
	LabelSetText(FrameWindow.."FrameHealthPercentage", towstring(healthPercent))

	-- Final step: set foreground and background colors
	WindowSetTintColor(FrameWindow.."FrameBarInnerForeground", R, G, B)
	WindowSetTintColor(FrameWindow.."FrameBarInnerBackground", 0, 0, 0)
end

--------------------------------------------------------------
-- function UpdateHealthBar()
-- Description:
--------------------------------------------------------------
function RVF_Modern:UpdateHealthBar()

	local FrameWindow = self:GetFrameWindow()

	-- First step: calculate bar height
	local healthPercent		= self.eHealthPercent or 100
	local Width, Height		= WindowGetDimensions(FrameWindow.."FrameBar")
	local HealthBarWidth	= self:GetSettings().HealthBarWidth

	h = Height*healthPercent/100

	WindowSetDimensions(FrameWindow.."FrameBarInner", HealthBarWidth, Height)
	WindowSetDimensions(FrameWindow.."FrameBarInnerForeground", HealthBarWidth, h)
	WindowSetDimensions(FrameWindow.."FrameBarInnerBackground", HealthBarWidth, Height-h)
end

--------------------------------------------------------------
-- function UpdateCareerIcon()
-- Description:
--------------------------------------------------------------
function RVF_Modern:UpdateCareerIcon()

	-- First step: get locals
	local healthPercent	= self.eHealthPercent or 100
	local TintColor
	local FrameWindow	= self:GetFrameWindow()

	if healthPercent > 0 then
		TintColor = self:GetSettings().careerIconTintColor
	else
		TintColor = self:GetSettings().deadCareerIconTintColor
	end

	-- Second step: check for TintColor
	if TintColor == nil then
		return
	end

	-- Third step: get RGBA components
	local R,G,B,A = math.floor(TintColor.R*255), math.floor(TintColor.G*255), math.floor(TintColor.B*255), TintColor.A

	-- Fourth step: set tint color
	WindowSetTintColor(FrameWindow.."CareerIcon", R, G, B)
	WindowSetAlpha(FrameWindow.."CareerIcon", A)

	-- Fifth step: check for career line 
	if self.eCareerLine ~= nil and self.eCareerLine ~=0 then

		-- : get locals
		local ShowCareerIcon = self:GetSettings().showTargetCareerIcon
		local texture, x, y = GetIconData(Icons.GetCareerIconIDFromCareerLine(self.eCareerLine))

		-- : set proper icon
		DynamicImageSetTexture(FrameWindow.."CareerIcon", texture, x, y)

		-- : process showing status
		WindowSetShowing(FrameWindow.."CareerIcon", ShowCareerIcon)
	else

		-- : hide icon
		WindowSetShowing(FrameWindow.."CareerIcon", false)
	end

--[[
	-- : get locals
	local ShowCareerIcon	= self:GetSettings().showTargetCareerIcon
	local texture			= nil
	local x					= 0
	local y					= 0
	local Width				= 32
	local Height			= 32
	local ScaleFactor		= 1

	-- Fifth step: check for career line 
	if self.eCareerLine ~= nil and self.eCareerLine ~=0 then

		-- : set texture information
		texture, x, y		= GetIconData(Icons.GetCareerIconIDFromCareerLine(self.eCareerLine))
		ScaleFactor			= 1

		-- : set proper icon
		DynamicImageSetTexture(FrameWindow.."CareerIcon", texture, x, y)
		WindowSetDimensions(FrameWindow.."CareerIcon", Width, Height)

	-- Sixth step: check if our target is personal pet
	elseif self.eIsPet then

		-- : set texture information
		local StateTexture	= CareerResource.m_CurrentDisplay.m_StateTextures
		texture				= StateTexture.MAIN_TEXTURE
		Width				= StateTexture[GameData.Player.Pet.stance].w
		Height				= StateTexture[GameData.Player.Pet.stance].h
		ScaleFactor			= 0.4

		-- : set proper icon
		DynamicImageSetTexture(FrameWindow.."CareerIcon", texture, x, y)
		DynamicImageSetTextureSlice(FrameWindow.."CareerIcon", StateTexture[GameData.Player.Pet.stance].texSlice)
		WindowSetDimensions(FrameWindow.."CareerIcon", Width, Height)

	-- Seventh step:	the target does not have any career information
	--					let's hide icon window
	else

		-- : hide icon
		ShowCareerIcon		= false
		ScaleFactor			= 1
	end

	-- Final step: process showing status
	WindowSetShowing(FrameWindow.."CareerIcon", ShowCareerIcon)
	WindowSetScale(FrameWindow.."CareerIcon", ScaleFactor)

]]
end

--------------------------------------------------------------
-- function UpdateAnimation()
-- Description:
--------------------------------------------------------------
function RVF_Modern:UpdateAnimation()

	-- First step: get locals
	local healthPercent	= self.eHealthPercent or 100
	local FrameWindow	= self:GetFrameWindow()
	local TintColor
	if healthPercent > 0 then
		TintColor = self:GetSettings().animationTintColor
	else
		TintColor = self:GetSettings().deadAnimationTintColor
	end

	-- Second step: check for TintColor
	if TintColor == nil then
		return
	end

	-- Third step: get RGBA components
	local R,G,B,A = math.floor(TintColor.R*255), math.floor(TintColor.G*255), math.floor(TintColor.B*255), TintColor.A

	-- Fourth step: set tint color
	WindowSetTintColor(FrameWindow.."Animation", R, G, B)
	WindowSetAlpha(FrameWindow.."Animation", A)
end
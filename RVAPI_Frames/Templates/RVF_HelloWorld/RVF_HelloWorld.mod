<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" >

	<!--
		IMPORTANT TO KNOW: please keep "name" attribute value same as your addon table.
		For example if you named your addon table like RVF_MyTemplate, you have to place name="RVF_MyTemplate" here too.
	-->
	<UiMod name="RVF_HelloWorld" version="1.03" date="07/09/2015" >
		<VersionSettings gameVersion="1.4.0" windowsVersion="1.0" savedVariablesVersion="1.0" />
		<Author name="MrAngel" email="" />
		<Description text="Demo template. Use it to start working on YOUR custom templates." />
		<Dependencies>
			<Dependency name="RVAPI_Frames" optional="false" forceEnable="true" />
		</Dependencies>

		<SavedVariables />

		<WARInfo>
			<Categories>
				<Category name="DEVELOPMENT" />
			</Categories>
			<Careers>
				<!-- Your career list here -->
			</Careers>
		</WARInfo>

		<Files>
			<File name="RVF_HelloWorld.lua" />
			<File name="RVF_HelloWorld.xml" />
		</Files>

		<OnInitialize>
			<CallFunction name="RVF_HelloWorld.Initialize" />
		</OnInitialize>

		<OnUpdate />

		<OnShutdown>
			<CallFunction name="RVF_HelloWorld.Shutdown" />
		</OnShutdown>
	</UiMod>

</ModuleFile>
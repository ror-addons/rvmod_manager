--[[----------------------------------------------------------------------------

	General introduction  

	This is the blank version of a RVAPI_Frames template. It was created to help
	developers start working at creating a new templates.

	Copy this addon in a separate folder. Please follow the RV addons policy
	so developers won't be stuck in recognizing of your addon role ;) 
	Let's say your new template name is "MyAddon". Than I suggest you to place everything
	under the \Interface\AddOns\RVF_MyAddon\ folder. Name your lua table with the same name - RVF_MyAddon.

	That tutorial have an extra comments, thoughts and suggestions, as well as extra EVENT functions which
	you probably will never use. Feel free to delete those and make your code clean.
	Please note also, that DEMO is not LUA optimized. You have to do some additional improvements
	as soon as you get the main idea of making of templates.

	The task of the current template is to show a target "Name" as an example. Expect to
	see some extra code, which are written to support that functionality.

----------------------------------------------------------------------------]]--

local RVCredits					= nil
local RVLicense					= "MIT License"
local RVProjectURL				= "http://www.returnofreckoning.com/forum/viewtopic.php?f=11&t=4534"
local RVRecentUpdates			= 
"09.07.2015 - v1.03 Release\n"..
"\t- Project official site location has been changed\n"..
"\t- Now it supports RV Mod Manager\n"..
"\n"..
"26.06.2010 - v1.02 Release\n"..
"\t- Signature logic has been removed\n"..
"\t- template name going to the API_RegisterTemplate function now"

RVF_HelloWorld = RVAPI_Frame:Subclass()

--------------------------------------------------------------
-- function Initialize()
-- Description: it is calling on the addon initialization.
--				By default it is trying to register template in the RVAPI_Frames system
--				I advice you modify that only in very special cases
--------------------------------------------------------------
function RVF_HelloWorld.Initialize()

	-- First step: register this template in the RVAPI_Frames.
	--             see "API_RegisterTemplate" for more details
	RVAPI_Frames.API_RegisterTemplate("RVF_HelloWorld", "Hello World")
	
	-- Second step: define event handlers
	RegisterEventHandler(SystemData.Events.ALL_MODULES_INITIALIZED, "RVF_HelloWorld.OnAllModulesInitialized")
end

--------------------------------------------------------------
-- function Shutdown()
-- Description: unregister template on shutdown 
--------------------------------------------------------------
function RVF_HelloWorld.Shutdown()

	-- First step: unregister all events
	UnregisterEventHandler(SystemData.Events.ALL_MODULES_INITIALIZED, "RVF_HelloWorld.OnAllModulesInitialized")

	-- Second step: unregister template from the frames manager
	RVAPI_Frames.API_UnRegisterTemplate("RVF_HelloWorld")
end

--------------------------------------------------------------
-- function OnAllModulesInitialized()
-- Description: event ALL_MODULES_INITIALIZED
-- We can start working with the RVAPI just then we sure they are all initialized
-- and ready to provide their services
--------------------------------------------------------------
function RVF_HelloWorld.OnAllModulesInitialized()

	-- Final step: register in the RV Mods Manager
	-- Please note the folowing:
	-- 1. always do this ON SystemData.Events.ALL_MODULES_INITIALIZED event
	-- 2. you don't need to add RVMOD_Manager to the dependency list
	-- 3. the registration code should be same as below, with your own function parameters
	-- 4. for more information please follow by project official site
	if RVMOD_Manager then
		RVMOD_Manager.API_RegisterAddon("RVF_HelloWorld", RVF_HelloWorld, RVF_HelloWorld.OnRVManagerCallback)
	end
end

--------------------------------------------------------------
-- function OnRVManagerCallback
-- Description:
--------------------------------------------------------------
function RVF_HelloWorld.OnRVManagerCallback(Self, Event, EventData)

	if	Event == RVMOD_Manager.Events.CREDITS_REQUESTED then

		return RVCredits

	elseif	Event == RVMOD_Manager.Events.LICENSE_REQUESTED then

		return RVLicense

	elseif	Event == RVMOD_Manager.Events.PROJECT_URL_REQUESTED then

		return RVProjectURL

	elseif	Event == RVMOD_Manager.Events.RECENT_UPDATES_REQUESTED then

		return RVRecentUpdates

	end
end

--------------------------------------------------------------
-- constructor Create()
-- Description: you can define your class properties at the second step
--------------------------------------------------------------
function RVF_HelloWorld:Create(frameName)

	-- First step: get an object from a parent method
	local frameObject = self:ParentCreate(frameName, "RVF_HelloWorldFrameTemplate", "Root")

	-- Second step: define variables. Put your class-global variables here
	--				they could look like:
	-- frameObject.eHealthPercent	= 100
	-- frameObject.eName			= L""
	-- etc.

	-- Final step: return object
	return frameObject
end

--------------------------------------------------------------
-- destructor Destroy()
-- Description: Remove that function if you have not anything to destroy/unload
--------------------------------------------------------------
function RVF_HelloWorld:Destroy()

	-- First step: your OnDestroy code here

	-- Final step: call parent destructor
	self:ParentDestroy()
end

--------------------------------------------------------------
-- function OnGetDefaultSettings()
-- Description: place your default settings here
--------------------------------------------------------------
function RVF_HelloWorld:OnGetDefaultSettings()

	-- First step: collect all default settings
	local Settings = {

		-- : lets set name showing to the TRUE by default
		showName		= true,
	}

	-- Final step: return result
	return Settings
end

--------------------------------------------------------------
-- function OnShowFrameWindow()
-- Description: it fire then your frame is about to show on a screen
--				You can remove that if not used
--------------------------------------------------------------
function RVF_HelloWorld:OnShowFrameWindow()

	-- your on show code here
end

--------------------------------------------------------------
-- function OnHideFrameWindow()
-- Description: it fire then your frame is about to hide
--				You can remove that if not used
--------------------------------------------------------------
function RVF_HelloWorld:OnHideFrameWindow()

	-- your on hide code here
end

--------------------------------------------------------------
-- function OnEntityUpdated()
-- Description: this is the entity updated event and the heart
--				of the addon. It is allow you to track all changes
--				are going on with your target
--				You can remove that if not used tho.
--------------------------------------------------------------
function RVF_HelloWorld:OnEntityUpdated(EntityData)

	-- as you can see it gives you EntityData parameter. This is the array (table in lua) of
	-- your target attributes like name, career, level, health, pets information and much more.
	-- The full list can be found at the project official site 
	-- in the types section or in the RVAPI_Entities project (look for the "local EntityData")

	-- I place that list here too, but keep in mind it could be outdated:

	-- * EntityId = entityId: the unique value in the 3D world. Always set at creation
	-- * EntityType = 0: one of the SystemData.TargetObjectType value
	-- * Name = L"": NPC, Player, static object name
	-- * Title = L"": ex. Flight Master, Healer
	-- * CareerName = L"": ex. White Lion, Archmage
	-- * CareerLine = 0
	-- * Level = 0
	-- * Tier = 0: 0 is the default value, 1-CHAMPION, 2-HERO, 3-LORD etc.
	-- * DifficultyMask = 0
	-- * IsNPC = false
	-- * IsPet = false
	-- * IsPVP = false: is flagged as PvP
	-- * ShowHealthBar = false
	-- * HitPointPercent = 0
	-- * ActionPointPercent = 0
	-- * MoraleLevel = 0
	-- * RelationshipColor = {r=0,g=0,b=0}
	-- * Online = false: available for friendly players only
	-- * ZoneNumber = 0
	-- * IsDistant = false: available for groups and warbands
	-- * IsInSameRegion = false: available for groups and warbands
	-- * IsGroupLeader = false: available for groups and warbands
	-- * IsAssistant = false: available for groups and warbands
	-- * IsMainAssist = false: available for groups and warbands
	-- * IsMasterLooter = false: available for groups and warbands
	-- * IsSelf = false
	-- * IsGroupMember = false: available for groups and warbands
	-- * IsScenarioGroupMember = false
	-- * IsWarbandMember = false: available for groups and warbands
	-- * IsTargeted = false: is targeted by the player
	-- * IsMouseOvered = false: is mouseovered by the player
	-- * Pet = {healthPercent = 0}
	-- * ConType = 0
	-- * MapPinType = 0
	-- * SigilEntryId = 0
	-- * RangeMin = RVAPI_Range.MIN_RANGE: minimum range value for the target
	-- * RangeMax = RVAPI_Range.MAX_RANGE: maximum range value for the target
	-- * GroupIndex = 0: available for groups and warbands
	-- * MemberIndex = 0: available for groups and warbands

	-- First step:	set new values
	--				lets set our target name
	local FrameWindow = self:GetFrameWindow()
	LabelSetText(FrameWindow.."Name", EntityData.Name)
end

--------------------------------------------------------------
-- function OnAttach()
-- Description:
--				Can be removed if not used
--------------------------------------------------------------
function RVF_HelloWorld:OnAttach()

	-- your on attach code here
end

--------------------------------------------------------------
-- function OnDetach()
-- Description:
--				Can be removed if not used
--------------------------------------------------------------
function RVF_HelloWorld:OnDetach()

	-- your on detach code here
end

--------------------------------------------------------------
-- function OnSetSetting()
-- Description: this is the set setting event. It hits every time
--				setting is about to change
--				Can be removed if not used
--------------------------------------------------------------
function RVF_HelloWorld:OnSetSetting(SettingName, Value)

	-- : lets check for out showName setting and do some magic according to it value
	if	SettingName == "showName" then

		-- : set window visibility
		local FrameWindow = self:GetFrameWindow()
		WindowSetShowing(FrameWindow.."Name", Value)
	end
end

--------------------------------------------------------------
-- function OnInitializeSettingsWindow()
-- Description: place your settings window definitions here.
--				In fact, settings window as custom as you want. No stricts or limitations.
--				Can be removed if not used
--------------------------------------------------------------
function RVF_HelloWorld:OnInitializeSettingsWindow()

	local SettingsWindow = self:GetSettingsWindow()

	-- First step: create window from template
	CreateWindowFromTemplate(SettingsWindow, "RVF_HelloWorldSettingsTemplate", "Root")
	LabelSetText(SettingsWindow.."TitleBarText", L"Hello World")
	ButtonSetText(SettingsWindow.."ButtonOK", L"OK")

	-- Second step: set additional controls
	--				Lets show some additional controls as an example
	LabelSetText(SettingsWindow.."LabelShowName", L"Show Name")
	LabelSetTextColor(SettingsWindow.."LabelShowName", 255, 255, 255)
end

--------------------------------------------------------------
-- function OnShowSettingsWindow()
-- Description: that hits only then settings window is about to open
-- 				it is recomended to place a code here to update settings window controls
--				Can be removed if not used
--------------------------------------------------------------
function RVF_HelloWorld:OnShowSettingsWindow()

	-- First step: get locals
	local SettingsWindow	= self:GetSettingsWindow()
	local CurrentSettings	= self:GetSettings()

	-- Second step: set show name check box
	ButtonSetPressedFlag(SettingsWindow.."CheckBoxShowName", CurrentSettings.showName)
end

--------------------------------------------------------------
-- function OnHideSettingsWindow()
-- Description: 
--				Can be removed if not used
--------------------------------------------------------------
function RVAPI_Frame:OnHideSettingsWindow()

	-- your on hide setting window code here
end

-------------------------------------------------------------
-- Window Events
-- Just a simple functions to support settings window functionality.
--------------------------------------------------------------

function RVF_HelloWorld.CloseSettings()

	-- : IMPORTANT! This is the only way to get SELF!
	local self = RVAPI_Frames.API_GetFrameObjectBySettingsWindow(WindowGetParent(SystemData.ActiveWindow.name))
	self:HideFrameSettings()
end

function RVF_HelloWorld.SetShowName()

	local self = RVAPI_Frames.API_GetFrameObjectBySettingsWindow(WindowGetParent(SystemData.ActiveWindow.name))
	local showName = self:GetSettings().showName
	local SettingsWindow = self:GetSettingsWindow()

	self:SetSetting("showName", not showName)
	ButtonSetPressedFlag(SettingsWindow.."CheckBoxShowName", not showName)
end
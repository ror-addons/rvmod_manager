local RVCredits					= "FuriCuri"
local RVLicense					= "MIT License"
local RVProjectURL				= "http://www.returnofreckoning.com/forum/viewtopic.php?f=11&t=4534"
local RVRecentUpdates			= 
"09.07.2015 - v1.01 Release\n"..
"\t- Project official site location has been changed\n"..
"\n"..
"25.07.2010 - v1.0 Release\n"..
"\t- Initial upload\n"..
"\t- "

RVF_Enemy = RVAPI_Frame:Subclass()

--------------------------------------------------------------
-- function Initialize()
-- Description:
--------------------------------------------------------------
function RVF_Enemy.Initialize()

	-- First step: register this template in the RVAPI_Frames - templates and frames manager
	RVAPI_Frames.API_RegisterTemplate("RVF_Enemy", "Enemy")

	-- Second step: define event handlers
	RegisterEventHandler(SystemData.Events.ALL_MODULES_INITIALIZED, "RVF_Enemy.OnAllModulesInitialized")
end

--------------------------------------------------------------
-- function Shutdown()
-- Description:
--------------------------------------------------------------
function RVF_Enemy.Shutdown()

	-- First step: unregister all events
	UnregisterEventHandler(SystemData.Events.ALL_MODULES_INITIALIZED, "RVF_Enemy.OnAllModulesInitialized")

	-- Second step:
	RVAPI_Frames.API_UnRegisterTemplate("RVF_Enemy")
end

--------------------------------------------------------------
-- function OnAllModulesInitialized()
-- Description: event ALL_MODULES_INITIALIZED
-- We can start working with the RVAPI just then we sure they are all initialized
-- and ready to provide their services
--------------------------------------------------------------
function RVF_Enemy.OnAllModulesInitialized()

	-- Final step: register in the RV Mods Manager
	-- Please note the folowing:
	-- 1. always do this ON SystemData.Events.ALL_MODULES_INITIALIZED event
	-- 2. you don't need to add RVMOD_Manager to the dependency list
	-- 3. the registration code should be same as below, with your own function parameters
	-- 4. for more information please follow by project official site
	if RVMOD_Manager then
		RVMOD_Manager.API_RegisterAddon("RVF_Enemy", RVF_Enemy, RVF_Enemy.OnRVManagerCallback)
	end
end

--------------------------------------------------------------
-- function OnRVManagerCallback
-- Description:
--------------------------------------------------------------
function RVF_Enemy.OnRVManagerCallback(Self, Event, EventData)

	if	Event == RVMOD_Manager.Events.CREDITS_REQUESTED then

		return RVCredits

	elseif	Event == RVMOD_Manager.Events.LICENSE_REQUESTED then

		return RVLicense

	elseif	Event == RVMOD_Manager.Events.PROJECT_URL_REQUESTED then

		return RVProjectURL

	elseif	Event == RVMOD_Manager.Events.RECENT_UPDATES_REQUESTED then

		return RVRecentUpdates

	end
end

--------------------------------------------------------------
-- constructor Create()
-- Description:
--------------------------------------------------------------
function RVF_Enemy:Create(frameName)

	-- First step: get an object from a parent method
	local frameObject = self:ParentCreate(frameName, "RVF_EnemyFrameTemplate", "Root")

	-- Second step: define "class" variables
	frameObject.eHealthPercent			= 100
	frameObject.eCareerLine				= 0
	frameObject.eCurrentColorBox		= ""

	-- Third step: register events
	WindowRegisterEventHandler(frameName, SystemData.Events.USER_SETTINGS_CHANGED, "RVF_Enemy.OnUserSettingsChanged")

	-- Final step: return object
	return frameObject
end

--------------------------------------------------------------
-- destructor Destroy()
-- Description:
--------------------------------------------------------------
function RVF_Enemy:Destroy()

	-- First step: get frame window
	local FrameWindow = self:GetFrameWindow()

	-- Second step: unregister events
	WindowUnregisterEventHandler(FrameWindow, SystemData.Events.USER_SETTINGS_CHANGED)

	-- Final step: call parent destructor
	self:ParentDestroy()
end

--------------------------------------------------------------
-- function OnGetDefaultSettings()
-- Description:
--------------------------------------------------------------
function RVF_Enemy:OnGetDefaultSettings()

	-- First step: collect all default settings
	local Settings = {

		showBGLayer				= true,						-- show background layer
		showHPBGLayer			= true,						-- show health background layer
		showHPLayer				= true,						-- show health layer
		showCareerIcon			= true,						-- show career icon

		UseGlobalScale			= true,
		ScaleFactor				= 1,						-- scale factor
		Layer					= Window.Layers.DEFAULT,	-- set background layer as default Window.Layers, WindowSetLayer(self.name, layer), WindowGetLayer(self.name)

		BGTintColor				= {R=1,		G=1,	B=1,	A=0.5},
		deadBGTintColor			= {R=0.25,	G=0.25,	B=0.25,	A=0.5},
		HPBGTintColor			= {R=0,		G=0,	B=0,	A=0.5},
		deadHPBGTintColor		= {R=1,		G=1,	B=1,	A=0.5},
		HPTintColor				= {R=1,		G=1,	B=1,	A=1},
		deadHPTintColor			= {R=0.5,	G=0.5,	B=0.5,	A=0.5},
		careerIconTintColor		= {R=1,		G=1,	B=1,	A=1},
		deadCareerIconTintColor	= {R=1,		G=1,	B=1,	A=0.75},
	}

	-- Final step: return result
	return Settings
end

--------------------------------------------------------------
-- function OnInitializeSettingsWindow()
-- Description: 
--------------------------------------------------------------
function RVF_Enemy:OnInitializeSettingsWindow()

	local SettingsWindow = self:GetSettingsWindow()

	-- First step: create window from template
	CreateWindowFromTemplate(SettingsWindow, "RVF_EnemySettingsTemplate", "Root")
	LabelSetText(SettingsWindow.."TitleBarText", L"Enemy")
	ButtonSetText(SettingsWindow.."ButtonOK", L"OK")

	-- Second step: set tab buttons
	ButtonSetText(SettingsWindow.."TabGeneral", L"General")
	ButtonSetText(SettingsWindow.."TabFrame", L"Frame")
	ButtonSetText(SettingsWindow.."TabCareerIcon", L"Career icon")

	-- Third step: set additional controls
	ComboBoxAddMenuItem(SettingsWindow.."TabWGeneralLayers", towstring("Background Layer (0)"))
	ComboBoxAddMenuItem(SettingsWindow.."TabWGeneralLayers", towstring("Default Layer (1)"))
	ComboBoxAddMenuItem(SettingsWindow.."TabWGeneralLayers", towstring("Secondary Layer (2)"))
	ComboBoxAddMenuItem(SettingsWindow.."TabWGeneralLayers", towstring("Popup Layer (3)"))
	ComboBoxAddMenuItem(SettingsWindow.."TabWGeneralLayers", towstring("Overlay Layer (4)"))

	LabelSetText(SettingsWindow.."TabWGeneralLabelUseGlobalScale", L"Use Global UI Scale")
	LabelSetTextColor(SettingsWindow.."TabWGeneralLabelUseGlobalScale", 255, 255, 255)
	LabelSetText(SettingsWindow.."TabWGeneralSliderBarScaleLabel", L"Scale")

	-- Fourth step: set Frame tab window
	LabelSetText(SettingsWindow.."TabWFrameBGCaption", L"Background")
	LabelSetText(SettingsWindow.."TabWFrameLabelBGEnabled", L"Enabled")
	LabelSetTextColor(SettingsWindow.."TabWFrameLabelBGEnabled", 255, 255, 255)

	LabelSetText(SettingsWindow.."TabWFrameHPBGCaption", L"Health Background")
	LabelSetText(SettingsWindow.."TabWFrameLabelHPBGEnabled", L"Enabled")
	LabelSetTextColor(SettingsWindow.."TabWFrameLabelHPBGEnabled", 255, 255, 255)

	LabelSetText(SettingsWindow.."TabWFrameHPCaption", L"Health")
	LabelSetText(SettingsWindow.."TabWFrameLabelHPEnabled", L"Enabled")
	LabelSetTextColor(SettingsWindow.."TabWFrameLabelHPEnabled", 255, 255, 255)

	-- Sixth step: set CareerIcon tab window
	LabelSetText(SettingsWindow.."TabWCareerIconCaption", L"Career Icon")
	LabelSetText(SettingsWindow.."TabWCareerIconLabelEnabled", L"Enabled")
	LabelSetTextColor(SettingsWindow.."TabWCareerIconLabelEnabled", 255, 255, 255)

	-- Final step: setup tabs visibility
	WindowSetShowing(SettingsWindow.."TabWGeneral", true)
	WindowSetShowing(SettingsWindow.."TabWFrame", false)
	WindowSetShowing(SettingsWindow.."TabWCareerIcon", false)

	ButtonSetPressedFlag(SettingsWindow.."TabGeneral", true)
	ButtonSetPressedFlag(SettingsWindow.."TabFrame", false)
	ButtonSetPressedFlag(SettingsWindow.."TabCareerIcon", false)
end

--------------------------------------------------------------
-- function OnShowSettingsWindow()
-- Description:
--------------------------------------------------------------
function RVF_Enemy:OnShowSettingsWindow()

	local CurrentSettings	= self:GetSettings()
	local SettingsWindow	= self:GetSettingsWindow()

	-- First step: update General TAB
	ButtonSetPressedFlag(SettingsWindow.."TabWGeneralCheckBoxUseGlobalScale", CurrentSettings.UseGlobalScale)
	SliderBarSetCurrentPosition(SettingsWindow.."TabWGeneralSliderBarScale", CurrentSettings.ScaleFactor-0.5)
	LabelSetText(SettingsWindow.."TabWGeneralSliderBarScaleEdit",	towstring(string.sub(CurrentSettings.ScaleFactor, 1, 3)))
	ComboBoxSetSelectedMenuItem(SettingsWindow.."TabWGeneralLayers", CurrentSettings.Layer+1)

	-- : update Frame TAB
	ButtonSetPressedFlag(SettingsWindow.."TabWFrameCheckBoxBGEnabled", CurrentSettings.showBGLayer)
	WindowSetTintColor(SettingsWindow.."TabWFrameBGLiveColorBorderForeground", CurrentSettings.BGTintColor.R*255, CurrentSettings.BGTintColor.G*255, CurrentSettings.BGTintColor.B*255) 
	WindowSetTintColor(SettingsWindow.."TabWFrameBGDeadColorBorderForeground", CurrentSettings.deadBGTintColor.R*255, CurrentSettings.deadBGTintColor.G*255, CurrentSettings.deadBGTintColor.B*255) 

	ButtonSetPressedFlag(SettingsWindow.."TabWFrameCheckBoxHPBGEnabled", CurrentSettings.showHPBGLayer)
	WindowSetTintColor(SettingsWindow.."TabWFrameHPBGLiveColorBorderForeground", CurrentSettings.HPBGTintColor.R*255, CurrentSettings.HPBGTintColor.G*255, CurrentSettings.HPBGTintColor.B*255) 
	WindowSetTintColor(SettingsWindow.."TabWFrameHPBGDeadColorBorderForeground", CurrentSettings.deadHPBGTintColor.R*255, CurrentSettings.deadHPBGTintColor.G*255, CurrentSettings.deadHPBGTintColor.B*255) 

	ButtonSetPressedFlag(SettingsWindow.."TabWFrameCheckBoxHPEnabled", CurrentSettings.showHPLayer)
	WindowSetTintColor(SettingsWindow.."TabWFrameHPLiveColorBorderForeground", CurrentSettings.HPTintColor.R*255, CurrentSettings.HPTintColor.G*255, CurrentSettings.HPTintColor.B*255) 
	WindowSetTintColor(SettingsWindow.."TabWFrameHPDeadColorBorderForeground", CurrentSettings.deadHPTintColor.R*255, CurrentSettings.deadHPTintColor.G*255, CurrentSettings.deadHPTintColor.B*255) 

	-- : update Career Icon TAB
	ButtonSetPressedFlag(SettingsWindow.."TabWCareerIconCheckBoxEnabled", CurrentSettings.showCareerIcon)
	WindowSetTintColor(SettingsWindow.."TabWCareerIconLiveColorBorderForeground", CurrentSettings.careerIconTintColor.R*255, CurrentSettings.careerIconTintColor.G*255, CurrentSettings.careerIconTintColor.B*255) 
	WindowSetTintColor(SettingsWindow.."TabWCareerIconDeadColorBorderForeground", CurrentSettings.deadCareerIconTintColor.R*255, CurrentSettings.deadCareerIconTintColor.G*255, CurrentSettings.deadCareerIconTintColor.B*255) 
end

--------------------------------------------------------------
-- function OnSetSetting()
-- Description: set settings event
--------------------------------------------------------------
function RVF_Enemy:OnSetSetting(SettingName, Value)

	-- : get frame window
	local FrameWindow = self:GetFrameWindow()

	-- set BGTintColor, deadBGTintColor
	if	SettingName == "BGTintColor" or
		SettingName == "deadBGTintColor" then

		-- : update reticle
		self:UpdateBGColor()
	end

	-- set HPBGTintColor, deadHPBGTintColor
	if	SettingName == "HPBGTintColor" or
		SettingName == "deadHPBGTintColor" then

		-- : update reticle
		self:UpdateHPBGColor()
	end

	-- set HPTintColor, deadHPTintColor
	if	SettingName == "HPTintColor" or
		SettingName == "deadHPTintColor" then

		-- : update reticle
		self:UpdateHPColor()
	end

	-- set careerIconTintColor, deadCareerIconTintColor
	if	SettingName == "careerIconTintColor" or
		SettingName == "deadCareerIconTintColor" then

		-- : update career icon
		self:UpdateCareerIconColor()
	end

	-- set showBGLayer
	if	SettingName == "showBGLayer" then
		WindowSetShowing(FrameWindow.."BG", Value)
	end

	-- set showBGLayer
	if	SettingName == "showHPBGLayer" then
		WindowSetShowing(FrameWindow.."HPBG", Value)
	end

	-- set showBGLayer
	if	SettingName == "showHPLayer" then
		self:UpdateHP()
	end

	-- set showCareerIcon
	if	SettingName == "showCareerIcon" then
		WindowSetShowing(FrameWindow.."CareerIcon", Value)
	end

	-- set ScaleFactor
	if	SettingName == "ScaleFactor" or
		SettingName == "UseGlobalScale" then

		-- set Scale
		self:UpdateScale()
	end

	-- set Layer
	if	SettingName == "Layer" then

		-- set Layer
		WindowSetLayer(FrameWindow, Value)
	end
end

--------------------------------------------------------------
-- function GetSettingByWindow()
-- Description: 
--------------------------------------------------------------
function RVF_Enemy:GetSettingByWindow(WindowName)

	local SettingsWindow = self:GetSettingsWindow()

	-- First step: calculate SettingName
	if WindowName == SettingsWindow.."TabWFrameBGLiveColor" then
		SettingName = "BGTintColor"
	elseif WindowName == SettingsWindow.."TabWFrameBGDeadColor" then
		SettingName = "deadBGTintColor"
	elseif WindowName == SettingsWindow.."TabWFrameHPBGLiveColor" then
		SettingName = "HPBGTintColor"
	elseif WindowName == SettingsWindow.."TabWFrameHPBGDeadColor" then
		SettingName = "deadHPBGTintColor"
	elseif WindowName == SettingsWindow.."TabWFrameHPLiveColor" then
		SettingName = "HPTintColor"
	elseif WindowName == SettingsWindow.."TabWFrameHPDeadColor" then
		SettingName = "deadHPTintColor"
	elseif WindowName == SettingsWindow.."TabWCareerIconLiveColor" then
		SettingName = "careerIconTintColor"
	elseif WindowName == SettingsWindow.."TabWCareerIconDeadColor" then
		SettingName = "deadCareerIconTintColor"
	end

	-- Final step: return result
	return SettingName
end

-------------------------------------------------------------
-- Window Events
--------------------------------------------------------------

function RVF_Enemy.CloseSettings()

	local self = RVAPI_Frames.API_GetFrameObjectBySettingsWindow(WindowGetParent(SystemData.ActiveWindow.name))
	self:HideFrameSettings()
end

function RVF_Enemy.OnClickTabGeneral()

	local self = RVAPI_Frames.API_GetFrameObjectBySettingsWindow(WindowGetParent(WindowGetParent(SystemData.ActiveWindow.name)))
	local SettingsWindow = self:GetSettingsWindow()

	WindowSetShowing(SettingsWindow.."TabWGeneral", true)
	WindowSetShowing(SettingsWindow.."TabWFrame", false)
	WindowSetShowing(SettingsWindow.."TabWCareerIcon", false)

	ButtonSetPressedFlag(SettingsWindow.."TabGeneral", true)
	ButtonSetPressedFlag(SettingsWindow.."TabFrame", false)
	ButtonSetPressedFlag(SettingsWindow.."TabCareerIcon", false)
end

function RVF_Enemy.OnClickTabFrame()

	local self = RVAPI_Frames.API_GetFrameObjectBySettingsWindow(WindowGetParent(WindowGetParent(SystemData.ActiveWindow.name)))
	local SettingsWindow = self:GetSettingsWindow()

	WindowSetShowing(SettingsWindow.."TabWGeneral", false)
	WindowSetShowing(SettingsWindow.."TabWFrame", true)
	WindowSetShowing(SettingsWindow.."TabWCareerIcon", false)

	ButtonSetPressedFlag(SettingsWindow.."TabGeneral", false)
	ButtonSetPressedFlag(SettingsWindow.."TabFrame", true)
	ButtonSetPressedFlag(SettingsWindow.."TabCareerIcon", false)
end

function RVF_Enemy.OnClickTabCareerIcon()

	local self = RVAPI_Frames.API_GetFrameObjectBySettingsWindow(WindowGetParent(WindowGetParent(SystemData.ActiveWindow.name)))
	local SettingsWindow = self:GetSettingsWindow()

	WindowSetShowing(SettingsWindow.."TabWGeneral", false)
	WindowSetShowing(SettingsWindow.."TabWFrame", false)
	WindowSetShowing(SettingsWindow.."TabWCareerIcon", true)

	ButtonSetPressedFlag(SettingsWindow.."TabGeneral", false)
	ButtonSetPressedFlag(SettingsWindow.."TabFrame", false)
	ButtonSetPressedFlag(SettingsWindow.."TabCareerIcon", true)
end

function RVF_Enemy.OnComboBoxLayerChange( choiceIndex )

	local ComboBoxWindow = SystemData.ActiveWindow.name
	local self = RVAPI_Frames.API_GetFrameObjectBySettingsWindow(WindowGetParent(WindowGetParent(ComboBoxWindow)))

	self:SetSetting("Layer", choiceIndex-1)
end

function RVF_Enemy.OnSlideScale( slidePos )

	local SliderWindow = SystemData.ActiveWindow.name
	local self = RVAPI_Frames.API_GetFrameObjectBySettingsWindow(WindowGetParent(WindowGetParent(SliderWindow)))
	local SettingsWindow = self:GetSettingsWindow()

	local ScaleFactor	= slidePos + 0.5
	self:SetSetting("ScaleFactor", ScaleFactor)
	LabelSetText(SettingsWindow.."TabWGeneralSliderBarScaleEdit", towstring(string.sub(ScaleFactor, 1, 3)))
end

function RVF_Enemy.OnMouseOverColorBox()

	local ColorBoxWindow	= SystemData.ActiveWindow.name
	local self				= RVAPI_Frames.API_GetFrameObjectBySettingsWindow(WindowGetParent(WindowGetParent(ColorBoxWindow)))
	local SettingsWindow	= self:GetSettingsWindow()

	Tooltips.CreateTextOnlyTooltip (ColorBoxWindow, nil)

	if ColorBoxWindow == SettingsWindow.."TabWFrameBGLiveColor" then
		Tooltips.SetTooltipText(1, 1, L"color")
	elseif ColorBoxWindow == SettingsWindow.."TabWFrameBGDeadColor" then
		Tooltips.SetTooltipText(1, 1, L"dead unit color")
	elseif ColorBoxWindow == SettingsWindow.."TabWFrameHPBGLiveColor" then
		Tooltips.SetTooltipText(1, 1, L"color")
	elseif ColorBoxWindow == SettingsWindow.."TabWFrameHPBGDeadColor" then
		Tooltips.SetTooltipText(1, 1, L"dead unit color")
	elseif ColorBoxWindow == SettingsWindow.."TabWFrameHPLiveColor" then
		Tooltips.SetTooltipText(1, 1, L"color")
	elseif ColorBoxWindow == SettingsWindow.."TabWFrameHPDeadColor" then
		Tooltips.SetTooltipText(1, 1, L"dead unit color")
	elseif ColorBoxWindow == SettingsWindow.."TabWCareerIconLiveColor" then
		Tooltips.SetTooltipText(1, 1, L"color")
	elseif ColorBoxWindow == SettingsWindow.."TabWCareerIconDeadColor" then
		Tooltips.SetTooltipText(1, 1, L"dead unit color")
	end
	Tooltips.SetTooltipColorDef(1, 1, Tooltips.COLOR_BODY)
	Tooltips.Finalize()

	local anchor = {Point="topleft", RelativeTo=ColorBoxWindow, RelativePoint="bottomleft", XOffset=0, YOffset=-8}
	Tooltips.AnchorTooltip(anchor)
	Tooltips.SetTooltipAlpha(1)
end

function RVF_Enemy.OnLButtonUpColorBox()

	-- First step: get the colorbox windo name and the right self: object
	local ColorBoxWindow	= SystemData.ActiveWindow.name
	local self				= RVAPI_Frames.API_GetFrameObjectBySettingsWindow(WindowGetParent(WindowGetParent(ColorBoxWindow)))
	local SettingsWindow	= self:GetSettingsWindow()

	-- Second step: check for the current ColorBox
	if self.eCurrentColorBox == ColorBoxWindow then

		-- Third step: close color dialog if opened
		RVAPI_ColorDialog.API_CloseDialog(true)
	else

		-- Fourth step: calculate input data
		local SettingName	= self:GetSettingByWindow(ColorBoxWindow)
		local Color			= self:GetSettings()[SettingName]
		local Layer			= WindowGetLayer(SettingsWindow) + 1
		if Layer > Window.Layers.OVERLAY then
			Layer			= Window.Layers.OVERLAY
		end

		-- Fifth step: open color dialog window
		RVAPI_ColorDialog.API_OpenDialog(self, RVF_Enemy.OnColorDialogCallback, true, Color.R*255, Color.G*255, Color.B*255, Color.A, Layer)

		-- Sixth step: save new ColorBox window
		self.eCurrentColorBox = ColorBoxWindow
	end
end

function RVF_Enemy:OnColorDialogCallback(Event, EventData)

	-- First step: check for the current ColorBox
	if self.eCurrentColorBox == "" then
		return
	end

	-- Second step: calculate setting name
	local SettingName = self:GetSettingByWindow(self.eCurrentColorBox)

	-- Third step: check for the setting name
	if SettingName == "" then
		return
	end

	-- Fourth step: get color
	local Color	= self:GetSettings()[SettingName]

	-- Fifth step: check for the right event
	if Event == RVAPI_ColorDialog.Events.COLOR_EVENT_UPDATED then

		-- : set new color values
		WindowSetTintColor(self.eCurrentColorBox.."BorderForeground", EventData.Red, EventData.Green, EventData.Blue) 
		Color.R	= EventData.Red / 255
		Color.G	= EventData.Green / 255
		Color.B	= EventData.Blue / 255
		Color.A	= EventData.Alpha
		self:SetSetting(SettingName, Color)

	elseif Event == RVAPI_ColorDialog.Events.COLOR_EVENT_CLOSED then

		-- : clear the current ColorBox window name
		self.eCurrentColorBox = ""
	end
end

function RVF_Enemy.SetUseGlobalScale()

	local ActiveWindow			= SystemData.ActiveWindow.name
	local self					= RVAPI_Frames.API_GetFrameObjectBySettingsWindow(WindowGetParent(WindowGetParent(ActiveWindow)))
	local UseGlobalScale		= self:GetSettings().UseGlobalScale

	self:SetSetting("UseGlobalScale", not UseGlobalScale)
	ButtonSetPressedFlag(ActiveWindow, not UseGlobalScale)
end

function RVF_Enemy.SetBGEnabled()

	local ActiveWindow			= SystemData.ActiveWindow.name
	local self					= RVAPI_Frames.API_GetFrameObjectBySettingsWindow(WindowGetParent(WindowGetParent(ActiveWindow)))
	local showBGLayer			= self:GetSettings().showBGLayer

	self:SetSetting("showBGLayer", not showBGLayer)
	ButtonSetPressedFlag(ActiveWindow, not showBGLayer)
end

function RVF_Enemy.SetHPBGEnabled()

	local ActiveWindow			= SystemData.ActiveWindow.name
	local self					= RVAPI_Frames.API_GetFrameObjectBySettingsWindow(WindowGetParent(WindowGetParent(ActiveWindow)))
	local showHPBGLayer			= self:GetSettings().showHPBGLayer

	self:SetSetting("showHPBGLayer", not showHPBGLayer)
	ButtonSetPressedFlag(ActiveWindow, not showHPBGLayer)
end

function RVF_Enemy.SetHPEnabled()

	local ActiveWindow			= SystemData.ActiveWindow.name
	local self					= RVAPI_Frames.API_GetFrameObjectBySettingsWindow(WindowGetParent(WindowGetParent(ActiveWindow)))
	local showHPLayer			= self:GetSettings().showHPLayer

	self:SetSetting("showHPLayer", not showHPLayer)
	ButtonSetPressedFlag(ActiveWindow, not showHPLayer)
end

function RVF_Enemy.SetCareerIconEnabled()

	local ActiveWindow			= SystemData.ActiveWindow.name
	local self					= RVAPI_Frames.API_GetFrameObjectBySettingsWindow(WindowGetParent(WindowGetParent(ActiveWindow)))
	local showCareerIcon		= self:GetSettings().showCareerIcon

	self:SetSetting("showCareerIcon", not showCareerIcon)
	ButtonSetPressedFlag(ActiveWindow, not showCareerIcon)
end

function RVF_Enemy.OnUserSettingsChanged()

	local self = RVAPI_Frames.API_GetFrameObjectByFrameWindow(SystemData.ActiveWindow.name)
	self:UpdateScale()
end

--------------------------------------------------------------
-- function OnEntityUpdated()
-- Description: entity updated event
--------------------------------------------------------------
function RVF_Enemy:OnEntityUpdated(EntityData)

	-- First step: calculate LiveDeadStatusChanged
	local LiveDeadStatusChanged =	(self.eHealthPercent > 0 and EntityData.HitPointPercent == 0) or 
									(self.eHealthPercent == 0 and EntityData.HitPointPercent > 0)

	-- Second step: save new values
	self.eHealthPercent	= EntityData.HitPointPercent
	self.eCareerLine	= EntityData.CareerLine

	-- Third step: update frame elements
	self:UpdateHP()
	self:UpdateCareerIcon()

	-- Final step: check live/dead status
	if LiveDeadStatusChanged then
		self:UpdateBGColor()
		self:UpdateHPBGColor()
		self:UpdateHPColor()
		self:UpdateCareerIconColor()
	end
end

--------------------------------------------------------------
-- function UpdateScale()
-- Description:
--------------------------------------------------------------
function RVF_Enemy:UpdateScale()

	-- First step: get frame window
	local FrameWindow = self:GetFrameWindow()

	-- Second step: get scale factor
	local ScaleFactor
	if self:GetSettings().UseGlobalScale then
		ScaleFactor = InterfaceCore.GetScale()
	else
		ScaleFactor = self:GetSettings().ScaleFactor or 1
	end

	-- Third step: apply scale factor
	WindowSetScale(FrameWindow, ScaleFactor*1)
	WindowSetScale(FrameWindow.."CareerIcon", ScaleFactor*0.9)
end

--------------------------------------------------------------
-- function UpdateHP()
-- Description:
--------------------------------------------------------------
function RVF_Enemy:UpdateHP()

	-- First step: get locals
	local healthPercent	= self.eHealthPercent or 100
	local FrameWindow	= self:GetFrameWindow()
	local ShowHPLayer	= self:GetSettings().showHPLayer

	-- Second step: calculate texture index
	local TextureIndex = math.floor(healthPercent / 20 + 0.5)

	-- Third step: update HP layet texture
	if (TextureIndex > 0)
	then
		DynamicImageSetTexture (FrameWindow.."HP", "TextureEnemyHP"..TextureIndex, 0, 0)
	end

	-- Final step: set layer visibility
	WindowSetShowing (FrameWindow.."HP", TextureIndex > 0 and ShowHPLayer)
end

--------------------------------------------------------------
-- function UpdateCareerIcon()
-- Description:
--------------------------------------------------------------
function RVF_Enemy:UpdateCareerIcon()

	-- First step: get frame window
	local FrameWindow = self:GetFrameWindow()

	-- Second step: check for career line 
	if self.eCareerLine ~= nil and self.eCareerLine ~=0 then

		-- Third step: get locals
		local ShowCareerIcon = self:GetSettings().showCareerIcon
		local texture, x, y = GetIconData(Icons.GetCareerIconIDFromCareerLine(self.eCareerLine))

		-- Fourth step: set proper icon
		DynamicImageSetTexture(FrameWindow.."CareerIcon", texture, x, y)

		-- Fifth step: process showing status
		WindowSetShowing(FrameWindow.."CareerIcon", ShowCareerIcon)
	else

		-- Sixth step: hide icon
		WindowSetShowing(FrameWindow.."CareerIcon", false)
	end
end

--------------------------------------------------------------
-- function UpdateBGColor()
-- Description:
--------------------------------------------------------------
function RVF_Enemy:UpdateBGColor()

	-- First step: get frame window
	local FrameWindow = self:GetFrameWindow()

	-- Second step: get background color by health
	local healthPercent = self.eHealthPercent or 100
	local TintColor
	if healthPercent > 0 then
		TintColor = self:GetSettings().BGTintColor
	else
		TintColor = self:GetSettings().deadBGTintColor
	end

	-- Third step: check for TintColor
	if TintColor ~= nil then

		-- Fourth step: get RGBA components
		local R,G,B,A = math.floor(TintColor.R*255), math.floor(TintColor.G*255), math.floor(TintColor.B*255), TintColor.A

		-- Fifth step: set tint color for the background and a bars
		WindowSetTintColor(FrameWindow.."BG", R, G, B)
		WindowSetAlpha(FrameWindow.."BG", A)
	end
end

--------------------------------------------------------------
-- function UpdateHPBGColor()
-- Description:
--------------------------------------------------------------
function RVF_Enemy:UpdateHPBGColor()

	-- First step: get frame window
	local FrameWindow = self:GetFrameWindow()

	-- Second step: get background color by health
	local healthPercent = self.eHealthPercent or 100
	local TintColor
	if healthPercent > 0 then
		TintColor = self:GetSettings().HPBGTintColor
	else
		TintColor = self:GetSettings().deadHPBGTintColor
	end

	-- Third step: check for TintColor
	if TintColor ~= nil then

		-- Fourth step: get RGBA components
		local R,G,B,A = math.floor(TintColor.R*255), math.floor(TintColor.G*255), math.floor(TintColor.B*255), TintColor.A

		-- Fifth step: set tint color for the background and a bars
		WindowSetTintColor(FrameWindow.."HPBG", R, G, B)
		WindowSetAlpha(FrameWindow.."HPBG", A)
	end
end

--------------------------------------------------------------
-- function UpdateHPColor()
-- Description:
--------------------------------------------------------------
function RVF_Enemy:UpdateHPColor()

	-- First step: get frame window
	local FrameWindow = self:GetFrameWindow()

	-- Second step: get background color by health
	local healthPercent = self.eHealthPercent or 100
	local TintColor
	if healthPercent > 0 then
		TintColor = self:GetSettings().HPTintColor
	else
		TintColor = self:GetSettings().deadHPTintColor
	end

	-- Third step: check for TintColor
	if TintColor ~= nil then

		-- Fourth step: get RGBA components
		local R,G,B,A = math.floor(TintColor.R*255), math.floor(TintColor.G*255), math.floor(TintColor.B*255), TintColor.A

		-- Fifth step: set tint color for the background and a bars
		WindowSetTintColor(FrameWindow.."HP", R, G, B)
		WindowSetAlpha(FrameWindow.."HP", A)
	end
end

--------------------------------------------------------------
-- function UpdateCareerIconColor()
-- Description:
--------------------------------------------------------------
function RVF_Enemy:UpdateCareerIconColor()

	-- First step: get frame window
	local FrameWindow = self:GetFrameWindow()

	-- Second step: get tint color by health
	local healthPercent = self.eHealthPercent or 100
	local TintColor
	if healthPercent > 0 then
		TintColor = self:GetSettings().careerIconTintColor
	else
		TintColor = self:GetSettings().deadCareerIconTintColor
	end

	-- Third step: check for TintColor
	if TintColor == nil then
		return
	end

	-- Fourth step: get RGBA components
	local R,G,B,A = math.floor(TintColor.R*255), math.floor(TintColor.G*255), math.floor(TintColor.B*255), TintColor.A

	-- Fifth step: set tint color
	WindowSetTintColor(FrameWindow.."CareerIcon", R, G, B)
	WindowSetAlpha(FrameWindow.."CareerIcon", A)
end
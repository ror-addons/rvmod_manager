local RVCredits					= "silverq"
local RVLicense					= "MIT License"
local RVProjectURL				= "http://www.returnofreckoning.com/forum/viewtopic.php?f=11&t=4534"
local RVRecentUpdates			= 
"09.07.2015 - v1.06 Release\n"..
"\t- Project official site location has been changed\n"..
"\n"..
"25.08.2010 - v1.05 Release\n"..
"\t- Fixed an issue with showing the distances information\n"..
"\n"..
"25.07.2010 - v1.04 Release\n"..
"\t- Code clearance\n"..
"\t- Adaptation to the new distance system has been made\n"..
"\n"..
"24.02.2010 - v1.03 Release\n"..
"\t- Code clearance\n"..
"\t- Adapted to work with the RV Mods Manager v0.99"

RVF_Classic = RVAPI_Frame:Subclass()

--------------------------------------------------------------
-- function Initialize()
-- Description:
--------------------------------------------------------------
function RVF_Classic.Initialize()

	-- First step: register this template in the RVAPI_Frames - templates and frames manager
	RVAPI_Frames.API_RegisterTemplate("RVF_Classic", "Classic")

	-- Second step: define event handlers
	RegisterEventHandler(SystemData.Events.ALL_MODULES_INITIALIZED, "RVF_Classic.OnAllModulesInitialized")
end

--------------------------------------------------------------
-- function Shutdown()
-- Description:
--------------------------------------------------------------
function RVF_Classic.Shutdown()

	-- First step: unregister all events
	UnregisterEventHandler(SystemData.Events.ALL_MODULES_INITIALIZED, "RVF_Classic.OnAllModulesInitialized")

	-- Second step:
	RVAPI_Frames.API_UnRegisterTemplate("RVF_Classic")
end

--------------------------------------------------------------
-- function OnAllModulesInitialized()
-- Description: event ALL_MODULES_INITIALIZED
-- We can start working with the RVAPI just then we sure they are all initialized
-- and ready to provide their services
--------------------------------------------------------------
function RVF_Classic.OnAllModulesInitialized()

	-- Final step: register in the RV Mods Manager
	-- Please note the folowing:
	-- 1. always do this ON SystemData.Events.ALL_MODULES_INITIALIZED event
	-- 2. you don't need to add RVMOD_Manager to the dependency list
	-- 3. the registration code should be same as below, with your own function parameters
	-- 4. for more information please follow by project official site
	if RVMOD_Manager then
		RVMOD_Manager.API_RegisterAddon("RVF_Classic", RVF_Classic, RVF_Classic.OnRVManagerCallback)
	end
end

--------------------------------------------------------------
-- function OnRVManagerCallback
-- Description:
--------------------------------------------------------------
function RVF_Classic.OnRVManagerCallback(Self, Event, EventData)

	if	Event == RVMOD_Manager.Events.CREDITS_REQUESTED then

		return RVCredits

	elseif	Event == RVMOD_Manager.Events.LICENSE_REQUESTED then

		return RVLicense

	elseif	Event == RVMOD_Manager.Events.PROJECT_URL_REQUESTED then

		return RVProjectURL

	elseif	Event == RVMOD_Manager.Events.RECENT_UPDATES_REQUESTED then

		return RVRecentUpdates

	end
end

--------------------------------------------------------------
-- constructor Create()
-- Description:
--------------------------------------------------------------
function RVF_Classic:Create(frameName)

	-- First step: get an object from a parent method
	local frameObject = self:ParentCreate(frameName, "RVF_ClassicFrameTemplate", "Root")

	-- Second step: define variables
	frameObject.eHealthPercent	= 100
	frameObject.eName			= L""
	frameObject.eLevel			= 0
	frameObject.eTier			= 0
	frameObject.eRangeMax		= RVAPI_Range.Ranges.MAX_RANGE
	frameObject.eLastRange		= -1

	-- Sixth step: return object
	return frameObject
end

--------------------------------------------------------------
-- function OnGetDefaultSettings()
-- Description:
--------------------------------------------------------------
function RVF_Classic:OnGetDefaultSettings()

	-- First step: collect all default settings
	local Settings = {
		showTargetName		= true,	-- show name label
		showTargetRange		= true,	-- show range label
		showTargetLevel		= true, -- show target level
		showGradient		= true, -- show gradient health bar

		frameTintColor		= {R=1,		G=1,	B=1,	A=1},
		deadFrameTintColor	= {R=0.5,	G=0.5,	B=0.5,	A=1},
		barBkgTintColor		= {R=0.1,	G=0.1,	B=0.1,	A=1},
	}

	-- Final step: return result
	return Settings
end

--------------------------------------------------------------
-- function OnInitializeSettingsWindow()
-- Description: 
--------------------------------------------------------------
function RVF_Classic:OnInitializeSettingsWindow()

	local SettingsWindow = self:GetSettingsWindow()

	-- First step: create window from template
	CreateWindowFromTemplate(SettingsWindow, "RVF_ClassicSettingsTemplate", "Root")
	LabelSetText(SettingsWindow.."TitleBarText", L"Classic")
	ButtonSetText(SettingsWindow.."ButtonOK", L"OK")

	-- Second step: set additional controls
	LabelSetText(SettingsWindow.."LabelShowName", L"Show Name")
	LabelSetTextColor(SettingsWindow.."LabelShowName", 255, 255, 255)

	LabelSetText(SettingsWindow.."LabelShowLevel", L"Show Level")
	LabelSetTextColor(SettingsWindow.."LabelShowLevel", 255, 255, 255)

	LabelSetText(SettingsWindow.."LabelShowRange", L"Show Range")
	LabelSetTextColor(SettingsWindow.."LabelShowRange", 255, 255, 255)

	LabelSetText(SettingsWindow.."LabelShowGradient", L"Use gradient Health Bar")
	LabelSetTextColor(SettingsWindow.."LabelShowGradient", 255, 255, 255)

	LabelSetText(SettingsWindow.."LabelPreviewFrameTint", L"COLOR PREVIEW")
	LabelSetFont(SettingsWindow.."LabelPreviewFrameTint", "font_clear_large_bold", WindowUtils.FONT_DEFAULT_TEXT_LINESPACING)
	LabelSetText(SettingsWindow.."LabelFrameTintHint", L"frame color")
	LabelSetTextColor(SettingsWindow.."LabelFrameTintHint", 255, 255, 255)
	LabelSetText(SettingsWindow.."LabelFrameTintRed", L"Red")
	LabelSetText(SettingsWindow.."LabelFrameTintGreen", L"Green")
	LabelSetText(SettingsWindow.."LabelFrameTintBlue", L"Blue")
	LabelSetText(SettingsWindow.."LabelFrameTintAlpha", L"Alpha")

	LabelSetText(SettingsWindow.."LabelPreviewDeadFrameTint", L"COLOR PREVIEW")
	LabelSetFont(SettingsWindow.."LabelPreviewDeadFrameTint", "font_clear_large_bold", WindowUtils.FONT_DEFAULT_TEXT_LINESPACING)
	LabelSetText(SettingsWindow.."LabelDeadFrameTintHint", L"dead unit color")
	LabelSetTextColor(SettingsWindow.."LabelDeadFrameTintHint", 255, 255, 255)
	LabelSetText(SettingsWindow.."LabelDeadFrameTintRed", L"Red")
	LabelSetText(SettingsWindow.."LabelDeadFrameTintGreen", L"Green")
	LabelSetText(SettingsWindow.."LabelDeadFrameTintBlue", L"Blue")
	LabelSetText(SettingsWindow.."LabelDeadFrameTintAlpha", L"Alpha")
end

--------------------------------------------------------------
-- function OnShowSettingsWindow()
-- Description:
--------------------------------------------------------------
function RVF_Classic:OnShowSettingsWindow()

	local CurrentSettings	= self:GetSettings()
	local SettingsWindow	= self:GetSettingsWindow()

	-- First step: show  information
	ButtonSetPressedFlag(SettingsWindow.."CheckBoxShowName", CurrentSettings.showTargetName)
	ButtonSetPressedFlag(SettingsWindow.."CheckBoxShowLevel", CurrentSettings.showTargetLevel)
	ButtonSetPressedFlag(SettingsWindow.."CheckBoxShowRange", CurrentSettings.showTargetRange)
	ButtonSetPressedFlag(SettingsWindow.."CheckBoxShowGradient", CurrentSettings.showGradient)

	local FrameTintRed			= math.floor(CurrentSettings.frameTintColor.R*255)
	local FrameTintGreen		= math.floor(CurrentSettings.frameTintColor.G*255)
	local FrameTintBlue			= math.floor(CurrentSettings.frameTintColor.B*255)
	local FrameTintAlpha		= math.floor(CurrentSettings.frameTintColor.A*255)

	LabelSetTextColor(SettingsWindow.."LabelPreviewFrameTint", FrameTintRed, FrameTintGreen, FrameTintBlue)
	LabelSetText(SettingsWindow.."EditFrameTintRed",		towstring(FrameTintRed))
	LabelSetText(SettingsWindow.."EditFrameTintGreen",	towstring(FrameTintGreen))
	LabelSetText(SettingsWindow.."EditFrameTintBlue",		towstring(FrameTintBlue))
	LabelSetText(SettingsWindow.."EditFrameTintAlpha",	towstring(FrameTintAlpha))

	SliderBarSetCurrentPosition(SettingsWindow.."SliderBarFrameTintRed", CurrentSettings.frameTintColor.R)
	SliderBarSetCurrentPosition(SettingsWindow.."SliderBarFrameTintGreen", CurrentSettings.frameTintColor.G)
	SliderBarSetCurrentPosition(SettingsWindow.."SliderBarFrameTintBlue", CurrentSettings.frameTintColor.B)
	SliderBarSetCurrentPosition(SettingsWindow.."SliderBarFrameTintAlpha", CurrentSettings.frameTintColor.A)

	local DeadFrameTintRed		= math.floor(CurrentSettings.deadFrameTintColor.R*255)
	local DeadFrameTintGreen	= math.floor(CurrentSettings.deadFrameTintColor.G*255)
	local DeadFrameTintBlue		= math.floor(CurrentSettings.deadFrameTintColor.B*255)
	local DeadFrameTintAlpha	= math.floor(CurrentSettings.deadFrameTintColor.A*255)

	LabelSetTextColor(SettingsWindow.."LabelPreviewDeadFrameTint", DeadFrameTintRed, DeadFrameTintGreen, DeadFrameTintBlue)
	LabelSetText(SettingsWindow.."EditDeadFrameTintRed",		towstring(DeadFrameTintRed))
	LabelSetText(SettingsWindow.."EditDeadFrameTintGreen",	towstring(DeadFrameTintGreen))
	LabelSetText(SettingsWindow.."EditDeadFrameTintBlue",		towstring(DeadFrameTintBlue))
	LabelSetText(SettingsWindow.."EditDeadFrameTintAlpha",	towstring(DeadFrameTintAlpha))

	SliderBarSetCurrentPosition(SettingsWindow.."SliderBarDeadFrameTintRed", CurrentSettings.deadFrameTintColor.R)
	SliderBarSetCurrentPosition(SettingsWindow.."SliderBarDeadFrameTintGreen", CurrentSettings.deadFrameTintColor.G)
	SliderBarSetCurrentPosition(SettingsWindow.."SliderBarDeadFrameTintBlue", CurrentSettings.deadFrameTintColor.B)
	SliderBarSetCurrentPosition(SettingsWindow.."SliderBarDeadFrameTintAlpha", CurrentSettings.deadFrameTintColor.A)
end

--------------------------------------------------------------
-- function OnSetSetting()
-- Description: set settings event
--------------------------------------------------------------
function RVF_Classic:OnSetSetting(SettingName, Value)

	local FrameWindow = self:GetFrameWindow()

	-- set frameTintColor, deadFrameTintColor, barBkgTintColor
	if	SettingName == "frameTintColor" or
		SettingName == "deadFrameTintColor" or
		SettingName == "barBkgTintColor" then

		-- : update reticle
		self:UpdateReticle()
	end

	-- set showTargetName
	if	SettingName == "showTargetName" then
		WindowSetShowing(FrameWindow.."FrameName", Value)
	end

	-- set showTargetLevel
	if	SettingName == "showTargetLevel" then

		-- : update name
		self:UpdateName()
	end

	-- set showTargetRange
	if	SettingName == "showTargetRange" then
		WindowSetShowing(FrameWindow.."FrameRange", Value)
	end
end

-------------------------------------------------------------
-- Window Events
--------------------------------------------------------------

function RVF_Classic.CloseSettings()

	local self = RVAPI_Frames.API_GetFrameObjectBySettingsWindow(WindowGetParent(SystemData.ActiveWindow.name))
	self:HideFrameSettings()
end

function RVF_Classic.SetFrameTintRed( slidePos )

	local self = RVAPI_Frames.API_GetFrameObjectBySettingsWindow(WindowGetParent(SystemData.ActiveWindow.name))
	local TintColor	= self:GetSettings().frameTintColor
	local SettingsWindow = self:GetSettingsWindow()
	TintColor.R		= slidePos

	self:SetSetting("frameTintColor", TintColor)

	LabelSetTextColor(SettingsWindow.."LabelPreviewFrameTint", math.floor(TintColor.R*255), math.floor(TintColor.G*255), math.floor(TintColor.B*255))
	LabelSetText(SettingsWindow.."EditFrameTintRed", towstring(math.floor(TintColor.R*255)))
end

function RVF_Classic.SetFrameTintGreen( slidePos )

	local self = RVAPI_Frames.API_GetFrameObjectBySettingsWindow(WindowGetParent(SystemData.ActiveWindow.name))
	local TintColor	= self:GetSettings().frameTintColor
	local SettingsWindow = self:GetSettingsWindow()
	TintColor.G		= slidePos

	self:SetSetting("frameTintColor", TintColor)

	LabelSetTextColor(SettingsWindow.."LabelPreviewFrameTint", math.floor(TintColor.R*255), math.floor(TintColor.G*255), math.floor(TintColor.B*255))
	LabelSetText(SettingsWindow.."EditFrameTintGreen", towstring(math.floor(TintColor.G*255)))
end

function RVF_Classic.SetFrameTintBlue( slidePos )

	local self = RVAPI_Frames.API_GetFrameObjectBySettingsWindow(WindowGetParent(SystemData.ActiveWindow.name))
	local TintColor	= self:GetSettings().frameTintColor
	local SettingsWindow = self:GetSettingsWindow()
	TintColor.B		= slidePos

	self:SetSetting("frameTintColor", TintColor)

	LabelSetTextColor(SettingsWindow.."LabelPreviewFrameTint", math.floor(TintColor.R*255), math.floor(TintColor.G*255), math.floor(TintColor.B*255))
	LabelSetText(SettingsWindow.."EditFrameTintBlue", towstring(math.floor(TintColor.B*255)))
end

function RVF_Classic.SetFrameTintAlpha( slidePos )

	local self = RVAPI_Frames.API_GetFrameObjectBySettingsWindow(WindowGetParent(SystemData.ActiveWindow.name))
	local TintColor	= self:GetSettings().frameTintColor
	local SettingsWindow = self:GetSettingsWindow()
	TintColor.A		= slidePos

	self:SetSetting("frameTintColor", TintColor)

	LabelSetTextColor(SettingsWindow.."LabelPreviewFrameTint", math.floor(TintColor.R*255), math.floor(TintColor.G*255), math.floor(TintColor.B*255))
	LabelSetText(SettingsWindow.."EditFrameTintAlpha", towstring(math.floor(TintColor.A*255)))
end

function RVF_Classic.SetDeadFrameTintRed( slidePos )

	local self = RVAPI_Frames.API_GetFrameObjectBySettingsWindow(WindowGetParent(SystemData.ActiveWindow.name))
	local TintColor	= self:GetSettings().deadFrameTintColor
	local SettingsWindow = self:GetSettingsWindow()
	TintColor.R		= slidePos

	self:SetSetting("deadFrameTintColor", TintColor)

	LabelSetTextColor(SettingsWindow.."LabelPreviewDeadFrameTint", math.floor(TintColor.R*255), math.floor(TintColor.G*255), math.floor(TintColor.B*255))
	LabelSetText(SettingsWindow.."EditDeadFrameTintRed", towstring(math.floor(TintColor.R*255)))
end

function RVF_Classic.SetDeadFrameTintGreen( slidePos )

	local self = RVAPI_Frames.API_GetFrameObjectBySettingsWindow(WindowGetParent(SystemData.ActiveWindow.name))
	local TintColor	= self:GetSettings().deadFrameTintColor
	local SettingsWindow = self:GetSettingsWindow()
	TintColor.G		= slidePos

	self:SetSetting("deadFrameTintColor", TintColor)

	LabelSetTextColor(SettingsWindow.."LabelPreviewDeadFrameTint", math.floor(TintColor.R*255), math.floor(TintColor.G*255), math.floor(TintColor.B*255))
	LabelSetText(SettingsWindow.."EditDeadFrameTintGreen", towstring(math.floor(TintColor.G*255)))
end

function RVF_Classic.SetDeadFrameTintBlue( slidePos )

	local self = RVAPI_Frames.API_GetFrameObjectBySettingsWindow(WindowGetParent(SystemData.ActiveWindow.name))
	local TintColor	= self:GetSettings().deadFrameTintColor
	local SettingsWindow = self:GetSettingsWindow()
	TintColor.B		= slidePos

	self:SetSetting("deadFrameTintColor", TintColor)

	LabelSetTextColor(SettingsWindow.."LabelPreviewDeadFrameTint", math.floor(TintColor.R*255), math.floor(TintColor.G*255), math.floor(TintColor.B*255))
	LabelSetText(SettingsWindow.."EditDeadFrameTintBlue", towstring(math.floor(TintColor.B*255)))
end

function RVF_Classic.SetDeadFrameTintAlpha( slidePos )

	local self = RVAPI_Frames.API_GetFrameObjectBySettingsWindow(WindowGetParent(SystemData.ActiveWindow.name))
	local TintColor	= self:GetSettings().deadFrameTintColor
	local SettingsWindow = self:GetSettingsWindow()
	TintColor.A		= slidePos

	self:SetSetting("deadFrameTintColor", TintColor)

	LabelSetTextColor(SettingsWindow.."LabelPreviewDeadFrameTint", math.floor(TintColor.R*255), math.floor(TintColor.G*255), math.floor(TintColor.B*255))
	LabelSetText(SettingsWindow.."EditDeadFrameTintAlpha", towstring(math.floor(TintColor.A*255)))
end

function RVF_Classic.SetShowName()

	local self = RVAPI_Frames.API_GetFrameObjectBySettingsWindow(WindowGetParent(SystemData.ActiveWindow.name))
	local showTargetName = self:GetSettings().showTargetName
	local SettingsWindow = self:GetSettingsWindow()

	self:SetSetting("showTargetName", not showTargetName)
	ButtonSetPressedFlag(SettingsWindow.."CheckBoxShowName", not showTargetName)
end

function RVF_Classic.SetShowLevel()

	local self = RVAPI_Frames.API_GetFrameObjectBySettingsWindow(WindowGetParent(SystemData.ActiveWindow.name))
	local showTargetLevel = self:GetSettings().showTargetLevel
	local SettingsWindow = self:GetSettingsWindow()

	self:SetSetting("showTargetLevel", not showTargetLevel)
	ButtonSetPressedFlag(SettingsWindow.."CheckBoxShowLevel", not showTargetLevel)
end

function RVF_Classic.SetShowRange()

	local self = RVAPI_Frames.API_GetFrameObjectBySettingsWindow(WindowGetParent(SystemData.ActiveWindow.name))
	local showTargetRange = self:GetSettings().showTargetRange
	local SettingsWindow = self:GetSettingsWindow()

	self:SetSetting("showTargetRange", not showTargetRange)
	ButtonSetPressedFlag(SettingsWindow.."CheckBoxShowRange", not showTargetRange)
end

function RVF_Classic.SetShowGradient()

	local self = RVAPI_Frames.API_GetFrameObjectBySettingsWindow(WindowGetParent(SystemData.ActiveWindow.name))
	local showGradient = self:GetSettings().showGradient
	local SettingsWindow = self:GetSettingsWindow()

	self:SetSetting("showGradient", not showGradient)
	ButtonSetPressedFlag(SettingsWindow.."CheckBoxShowGradient", not showGradient)
end

function RVF_Classic.OnVerticalBarSizeUpdated()

	local self = RVAPI_Frames.API_GetFrameObjectByFrameWindow(WindowGetParent(WindowGetParent(SystemData.ActiveWindow.name)))
	self:UpdateHealthBar()
end

--------------------------------------------------------------
-- function OnEntityUpdated()
-- Description: entity updated event
--------------------------------------------------------------
function RVF_Classic:OnEntityUpdated(EntityData)

	-- First step: set new values
	self.eHealthPercent	= EntityData.HitPointPercent
	self.eName			= EntityData.Name
	self.eLevel			= EntityData.Level
	self.eTier			= EntityData.Tier
	self.eRangeMax		= EntityData.RangeMax

	-- Second step: update name
	self:UpdateName()

	-- Third step: update range
	self:UpdateRange()

	-- Fourth step: update reticle
	self:UpdateReticle()

	-- Fifth step: update health bar
	self:UpdateHealthBar()
end

--------------------------------------------------------------
-- function UpdateName()
-- Description:
--------------------------------------------------------------
function RVF_Classic:UpdateName()

	-- First step: get locals
	local Name			= self.eName or L""
	local Level			= self.eLevel or 0
	local Tier			= self.eTier or 0
	local FrameWindow	= self:GetFrameWindow()

	-- Second step: check for showTargetLevel flag
	if self:GetSettings().showTargetLevel then

		-- Third step: add more information
		Name = Name..L" ("..Level..( (Tier == 1 and L"+") or (Tier == 2 and L"++") or (Tier == 3 and L"+++") or L"" )..L")"
	end

	-- Final step: send a name value to window
	LabelSetText(FrameWindow.."FrameName", Name)
end

--------------------------------------------------------------
-- function UpdateRange()
-- Description:
--------------------------------------------------------------
function RVF_Classic:UpdateRange()

	-- First step: get locals
	local range			= self.eRangeMax or RVAPI_Range.Ranges.MAX_RANGE
	local FrameWindow	= self:GetFrameWindow()

	-- Second step: check range with a last value
	if range ~= self.eLastRange then

		-- : define locals
		local Width		= 0
		local Height	= 0
		local Message	= L""

		-- : calculate dimensions
		if range > 200 then
			Width		= 42
			Height		= 64
		elseif range <= 5 then
			Width		= 118
			Height		= 178
		else
			Width		= 120-0.4*range
			Height		= 180-0.6*range
		end

		-- : define range message
		if range < RVAPI_Range.Ranges.MAX_RANGE then
			Message	= towstring(range)..L" ft"
		end

		-- : update information
		WindowSetDimensions(FrameWindow.."Frame", Width, Height)
		LabelSetText(FrameWindow.."FrameRange", Message)

		-- Final step: save the current range
		self.eLastRange = range
	end
end

--------------------------------------------------------------
-- function UpdateReticle()
-- Description:
--------------------------------------------------------------
function RVF_Classic:UpdateReticle()

	-- First step: get locals
	local healthPercent	= self.eHealthPercent or 100
	local FrameWindow	= self:GetFrameWindow()
	local TintColor
	if healthPercent > 0 then
		TintColor = self:GetSettings().frameTintColor
	else
		TintColor = self:GetSettings().deadFrameTintColor
	end

	-- Second step: check for TintColor
	if TintColor == nil then
		return
	end

	-- Third step: get RGBA components
	local R,G,B,A = math.floor(TintColor.R*255), math.floor(TintColor.G*255), math.floor(TintColor.B*255), TintColor.A

	-- Fourth step: set tint color
	WindowSetTintColor(FrameWindow, R, G, B)
	WindowSetAlpha(FrameWindow, A)

	-- Fifth step: set labels color
	LabelSetTextColor(FrameWindow.."FrameName", R, G, B)
	LabelSetTextColor(FrameWindow.."FrameRange", R, G, B)

	-- Sixth step: check for gradient status and set foreground color
	if self:GetSettings().showGradient then
		-- default color if >0.75, red if <0.25, gradient in between
		local p = math.max(healthPercent/100*2-0.5, 0)
		if p < 1 then
			R, G, B = 255*(1-p)+R*p, p*G, p*B
		end

		WindowSetTintColor(FrameWindow.."FrameBarForeground", R, G, B)
	else
		WindowSetTintColor(FrameWindow.."FrameBarForeground", R, G, B)
	end

	-- Final step: set background color
	-- TODO: (MrAngel) do we need settings for the background bar?
	WindowSetTintColor(FrameWindow.."FrameBarBackground", 0, 0, 0)
end

--------------------------------------------------------------
-- function UpdateHealthBar()
-- Description:
--------------------------------------------------------------
function RVF_Classic:UpdateHealthBar()

	local FrameWindow = self:GetFrameWindow()

	-- First step: calculate bar height
	local healthPercent = self.eHealthPercent or 100
	local Width, Height = WindowGetDimensions(FrameWindow.."FrameBar")
	h = Height*healthPercent/100
	WindowSetDimensions(FrameWindow.."FrameBarForeground", Width, h)
	WindowSetDimensions(FrameWindow.."FrameBarBackground", Width, Height-h)
end